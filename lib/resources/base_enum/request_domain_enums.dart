import 'package:flutter/foundation.dart';

class RequestDomainEnums {
  const RequestDomainEnums._();

  static RequestDomain apiMobile = RequestDomain(
    1,
    "https://sms-activate.org/stubs/apiMobile.php",
    "/stubs/apiMobile.php",
  );
  static RequestDomain handlerAuth = RequestDomain(
    2,
    "https://sms-activate.org/stubs/handler_auth.php",
    "/stubs/handler_auth.php",
  );
  static RequestDomain handlerApi = RequestDomain(
    3,
    "https://sms-activate.org/stubs/handler_api.php",
    "/stubs/handler_api.php",
  );
  static RequestDomain rent = RequestDomain(
    4,
    "https://sms-activate.org/stubs/rent.php",
    "/stubs/rent.php",
  );
  static RequestDomain handlerPayment = RequestDomain(
    5,
    "https://sms-activate.org/inc/pay/MobileAppPayments/handler_payment.php",
    "/inc/pay/MobileAppPayments/handler_payment.php",
  );
  static RequestDomain handlerTelegram = RequestDomain(
    6,
    "https://sms-activate.org/stubs/handler_api_telegram.php",
    "/stubs/handler_api_telegram.php",
  );
  static RequestDomain handlerSessionTelegram = RequestDomain(
    7,
    "https://bot.hostdarw.com/sessionCallback.php",
    "https://bot.hostdarw.com/sessionCallback.php",
  );
  // static RequestDomain apiMobile = RequestDomain(
  //   1,
  //   "https://test.uner.io/stubs/apiMobile.php",
  // );
  // static RequestDomain handlerAuth = RequestDomain(
  //   2,
  //   "https://test.uner.io/stubs/handler_auth.php",
  // );
  // static RequestDomain handlerApi = RequestDomain(
  //   3,
  //   "https://test.uner.io/stubs/handler_api.php",
  // );
  // static RequestDomain rent = RequestDomain(
  //   4,
  //   "https://test.uner.io/stubs/rent.php",
  // );
  // static RequestDomain handlerPayment = RequestDomain(
  //   5,
  //   "https://test.uner.io/inc/pay/MobileAppPayments/handler_payment.php",
  // );
  // static RequestDomain handlerTelegram = RequestDomain(
  //   6,
  //   "https://test.uner.io/stubs/handler_api_telegram.php",
  // );
  static List<RequestDomain> listRequestDomain = [
    apiMobile,
    handlerAuth,
    handlerApi,
    rent,
    handlerPayment,
    handlerSessionTelegram,
  ];
}

class RequestDomain {
  final int id;
  final String _domain;
  final String _relativeDomain;

  RequestDomain(
    this.id,
    this._domain,
    this._relativeDomain,
  );

  int getId() {
    return id;
  }

  String domain() {
    if (kDebugMode){
      return _domain;
    }else
    {
      return _relativeDomain;
    }
  }
}
