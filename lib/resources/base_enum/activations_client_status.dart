import 'package:sms_activate_https_client/resources/base_enum/activations_client_rent_status.dart';

class ActivationsClientStatus{
  const  ActivationsClientStatus._();

  static  StatusRent massageWasSent = StatusRent(1,  "Сообщить о готовности номера (смс на номер отправлено).", "Inform about the readiness of the number (SMS sent to the number).");
  static  StatusRent fakeSms = StatusRent(2, "Не нужно принимать смс.", "No need to receive SMS.");
  static  StatusRent requestOneMoreCode = StatusRent(3, "Запросить еще один код (бесплатно).", "Request another code (free).");
  static  StatusRent finish = StatusRent(6, "Завершить активацию.", "Finish the activation.");
  static  StatusRent cancel = StatusRent(8, "Сообщить о том, что номер использован и отменить активацию", "Report that the number has been used and cancel activation.");


  static List<StatusRent>activationStatus = [massageWasSent,fakeSms,requestOneMoreCode,finish,cancel];
}

class ActivationsServerStatus{
  const  ActivationsServerStatus._();

  static  StatusRent success = StatusRent(0,"Успешное изменение статуса активации.", "Successful change of activation status.");
  static  StatusRent error = StatusRent(1, "Ошибка изменение статуса.", "Error status change.");
  static List<StatusRent>activationStatus = [success,error];
}