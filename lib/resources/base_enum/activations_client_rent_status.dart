
class ActivationsClientRentStatus{
  const  ActivationsClientRentStatus._();

  static  StatusRent finish = StatusRent(1, "Завершить аренду.","Finish the rent.");
  static  StatusRent cancel = StatusRent(2, "Отменить аренду.", "Cancel the rent.");
  static List<StatusRent>activationStatus = [finish,cancel];
}

class ActivationsServerRentStatus{
  const  ActivationsServerRentStatus._();

  static  StatusRent success = StatusRent(0,"Успешное изменение статуса активации.", "Successful change of activation status.");
  static  StatusRent error = StatusRent(1, "Ошибка изменение статуса.", "Error status change.");
  static List<StatusRent>activationStatus = [success,error];
}

class StatusRent{
  final int id;
  final String ru;
  final String en;
  String? message;
  StatusRent(this.id, this.ru, this.en);

  int getId() {
    return id;
  }

  StatusRent setMessage(String text){
    message = text;
    return this;
  }
}