class HistoryType {
  String name = "successful";
  int index = 1;

  HistoryType([this.name = "successful", this.index = 1]);

  HistoryType.all() {
    name = "all";
    index = 0;
  }

  HistoryType.successful() {
    name = "successful";
    index = 1;
  }

  HistoryType.unsuccessful() {
    name = "cancel";
    index = 2;
  }

  HistoryType getType(int index) {
    if (index == HistoryType.all().index) {
      return HistoryType.all();
    } else if (index == HistoryType.unsuccessful().index) {
      return HistoryType.unsuccessful();
    } else {
      return HistoryType.successful();
    }
  }
}