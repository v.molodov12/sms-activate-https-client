

class PaymentsStatus{
  const  PaymentsStatus._();

  static  StatusPayment successPayment = StatusPayment("SUCCESS_PAYMENT", "Успешно.", "Success.");
  static  StatusPayment errorPayment = StatusPayment("ERROR_PAYMENT", "Ошибка.", "Error.");

  static List<StatusPayment>paymentStatus = [
    successPayment,errorPayment
  ];
}

class StatusPayment{
  final String name;
  final String ru;
  final String en;
  String? sms;

  StatusPayment(this.name, this.ru, this.en);
}