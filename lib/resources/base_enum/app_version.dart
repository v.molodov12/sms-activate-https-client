
class AppVersionType  {
  const  AppVersionType._();
  static  AppVersion ios = AppVersion(0, "SMS-Activate Ios", 2.5);
  //при переходе на обновлени 1.1.0  серверный нейминг осталслся 1.7 с одной плавающей цифрой 2.5 = 1.1.6
  static  AppVersion android = AppVersion(1, "SMS-Activate Android", 3.93);
  static  AppVersion desktop = AppVersion(2, "SMS-Activate Desktop", 1.6);
  // если делаем apk андройд для сайта, androidForSite изменяем на 1 иначе 0
  static  AppVersion androidForSite = AppVersion(3, "SMS-Activate Android for site", 0);
  static List<AppVersion>activationStatus = [
    desktop,
  ];
}

class AppVersion  {
  final int id;
  final String name;
  final double version;

  AppVersion(this.id,this.name,this.version,);
}
