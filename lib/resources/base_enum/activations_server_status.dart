
class ServerStatusActivations{
  const  ServerStatusActivations._();
  static  StatusServerActivation receivedSMS  = StatusServerActivation(2, "STATUS_OK");
  static  StatusServerActivation getMoreSMS  = StatusServerActivation(3, "STATUS_WAIT_RETRY");
  static  StatusServerActivation newActivation = StatusServerActivation(4, "STATUS_WAIT_CODE");
  static  StatusServerActivation activationFinish = StatusServerActivation(6, "STATUS_FINISH");
  static  StatusServerActivation activationCancel = StatusServerActivation(8, "STATUS_CANCEL");
  static  Map<int, StatusServerActivation> mapStatus = {2:receivedSMS,3:getMoreSMS,4:newActivation,6:activationFinish,8:activationCancel};

  static StatusServerActivation getStatus(int idStatus) {
    if(mapStatus[idStatus]!= null){
      return mapStatus[idStatus]!;
    }else{
      return mapStatus[2]! ;
    }
  }
}

class StatusServerActivation{
  final int id;
  String textId;
  StatusServerActivation(this.id, this.textId,);

  int getId() {
    return id;
  }
  int getTextID() {
    return id;
  }
}