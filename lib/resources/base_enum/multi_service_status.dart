
class MultiServiceStatus{
  const  MultiServiceStatus._();

  static  StatusMultiService SYNCED = StatusMultiService(1,);
  static  StatusMultiService DELETE = StatusMultiService(2,);
  static  StatusMultiService CHANGE = StatusMultiService(3,);
  static  StatusMultiService ADD = StatusMultiService(4,);
  static List<StatusMultiService> multiServiceStatus = [SYNCED,DELETE,CHANGE];
}

class StatusMultiService{
  final int id;
  StatusMultiService(this.id,);

  int getId() {
    return id;
  }
}