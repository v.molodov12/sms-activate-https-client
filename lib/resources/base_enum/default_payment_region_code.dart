class DefaultRegionCodeSystem {
  const DefaultRegionCodeSystem._();

  static DefaultRegionCode ru =
      DefaultRegionCode(1, "ru","RU");
  static DefaultRegionCode cn =
      DefaultRegionCode(2, "cn","CN");

  static Map<int, DefaultRegionCode> mapStatus = {
    1: ru,
    2: cn,
  };

  static DefaultRegionCode getStatus(int idStatus) {
    if (mapStatus[idStatus] != null) {
      return mapStatus[idStatus]!;
    } else {
      return mapStatus[2]!;
    }
  }
}

class DefaultRegionCode {
  final int id;
  String name;
  String name2;

  DefaultRegionCode(
    this.id,
    this.name,
    this.name2,
  );

  int getId() {
    return id;
  }

  String getTextID() {
    return name;
  }
  String getText2ID() {
    return name;
  }
}
