enum ActivationTypeNumber {
  sms,
  number,
  // voice
}

extension ActivationTypeNumberExtinsion on ActivationTypeNumber{
  String get indexType {
    switch(this){
      case ActivationTypeNumber.sms:
        return '0';
      case ActivationTypeNumber.number:
        return '1';
    // case ActivationTypeNumber.voice:
    //   return '2';
    }
  }

  String get indexName {
    switch(this){
      case ActivationTypeNumber.sms:
        // return S.current.verificationSms;
        return "СМС";
      case ActivationTypeNumber.number:
        // return S.current.verificationNumbers;
        return "Номеру";
    // case ActivationTypeNumber.voice:
    //   return 'Голосу';
    }
  }
}