
class MarketId  {
  const  MarketId._();
  static  TypeMarketId android = TypeMarketId(1, "android");
  static  TypeMarketId ios = TypeMarketId(2, "IOS");
  static  TypeMarketId desktop = TypeMarketId(6, "desktop");

  static List<TypeMarketId>activationStatus = [
    android,ios,desktop
  ];
}
class TypeMarketId  {
  final int id;
  final String name;

  int getId() {
    return id;
  }

  TypeMarketId(this.id, this.name);
}
