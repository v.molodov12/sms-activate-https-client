// import 'dart:io';
// import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:sms_activate/dependencyInjection/injector.dart';
// import 'package:sms_activate/resources/appTranslations/l10n/all_locales.dart';
//
// class LocaleRepository {
//   // final SharedPreferencesData spData;
//
//   static const locale = "locale";
//   static LocaleRepository? _instance;
//
//   factory LocaleRepository.getInstance() => _instance ??=
//       LocaleRepository._internal();
//
//   LocaleRepository._internal();
//
//   Future<bool> setLocale(final Locale localeData) async {
//     final sp = locator<SharedPreferences>();
//     final result = await sp.setString(
//       locale,
//       localeData.languageCode,
//     );
//     return result;
//   }
//
//   Future<Locale> getLocale() async {
//     try{
//       final sp = locator<SharedPreferences>();
//       String? value = sp.getString(locale);
//
//       Locale? platformLocale =
//           AllLocale.all[Platform.localeName.substring(0, 2)] ??
//               const Locale('en', 'English');
//
//       if (value != null) {
//         if (AllLocale.all[value] != null) {
//           Locale localeData = AllLocale.all[value]!;
//           return localeData;
//         } else {
//           return platformLocale;
//         }
//       } else {
//         return platformLocale;
//       }
//     } catch(ex) {
//       return const Locale('en', 'English');
//     }
//   }
// }
