class Operators {
  final int id;
  final String operatorName;
  final String idCountry;
  final String ru;
  final String en;
  final String cn;

  const Operators({
    required this.id,
    required this.operatorName,
    required this.idCountry,
    required this.ru,
    required this.en,
    required this.cn,
  });
}
