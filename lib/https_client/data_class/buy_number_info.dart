import 'package:equatable/equatable.dart';
import 'package:sms_activate_https_client/https_client/data_class/presentation_count_and_price_rent.dart';
import 'package:sms_activate_https_client/https_client/data_class/presentation_country.dart';
import 'package:sms_activate_https_client/https_client/data_class/presentation_service.dart';
import 'package:sms_activate_https_client/resources/base_enum/activation_type_number.dart';

class BuyNumberInfo extends Equatable {
  final String currentOperatorId;
  final RentalTimePeriod currentRentalSegment;
  final String currentFreePrice;
  final double countNumber;
  final ActivationTypeNumber activationType;
  final String cost;
  final PresentationService serviceInfo;
  final PresentationCountry countryInfo;

  const BuyNumberInfo(
      {
      required this.currentOperatorId,
      required this.currentRentalSegment,
      required this.currentFreePrice,
      required this.countNumber,
      required this.serviceInfo,
      required this.countryInfo,
      required this.cost,
      this.activationType = ActivationTypeNumber.sms,
      });

  BuyNumberInfo copyWith(
  {
    String? newCurrentOperatorId,
    RentalTimePeriod? newCurrentRentalSegment,
    String? newCurrentFreePrice,
    double? newCountNumber,
    ActivationTypeNumber? newActivationType,
    PresentationService? newServiceInfo,
    PresentationCountry? newCountryInfo,
    String? newCost,}
  ) {
    return BuyNumberInfo(
      currentOperatorId: newCurrentOperatorId ?? currentOperatorId,
      currentRentalSegment: newCurrentRentalSegment ?? currentRentalSegment,
      currentFreePrice: newCurrentFreePrice ?? currentFreePrice,
      countNumber: newCountNumber ?? countNumber,
      activationType: newActivationType ?? activationType,
      serviceInfo: newServiceInfo ?? serviceInfo,
      countryInfo: newCountryInfo ?? countryInfo,
      cost: newCost ?? cost,
    );
  }

  @override
  List<Object?> get props => [
    currentOperatorId,
        currentRentalSegment,
        currentFreePrice,
        countNumber,
        activationType,
        cost,
      ];

  Map<String, dynamic> toJson() => <String, dynamic>{
        "currentOperator": currentOperatorId,
        "currentRentalSegment": currentRentalSegment,
        "currentFreePrice": currentFreePrice,
        "countNumber": countNumber,
        "activationType": activationType.index,
        "cost": cost,
        "serviceInfo": serviceInfo,
        "countryInfo": countryInfo,
      };
}
