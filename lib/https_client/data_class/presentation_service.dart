import 'package:equatable/equatable.dart';
import 'package:sms_activate_https_client/resources/base_enum/service_groups.dart';

class PresentationService extends Equatable {
  final String shortName;
  final String serviceName;
  final String? keywords;
  final int sellTop;
  final double price;
  final double freePrice;
  final String forward;
  final int count;
  final bool onlyRent;
  final ServiceGroup? group;
  final bool firstElement;

  const PresentationService({
    required this.sellTop,
    required this.shortName,
    required this.serviceName,
    this.keywords,
    required this.price,
    required this.freePrice,
    required this.forward,
    required this.count,
    this.onlyRent = false,
    this.group,
    this.firstElement = false,
  });

  PresentationService copyWith({
    String? shortName,
    String? serviceName,
    String? keywords,
    int? sellTop,
    double? price,
    double? freePrice,
    String? forward,
    int? count,
    bool? onlyRent,
    ServiceGroup? group,
    bool? firstElement,
  }) {
    return PresentationService(
      shortName: shortName ?? this.shortName,
      serviceName: serviceName ?? this.serviceName,
      keywords: keywords ?? this.keywords,
      sellTop: sellTop ?? this.sellTop,
      price: price ?? this.price,
      freePrice: freePrice ?? this.freePrice,
      forward: forward ?? this.forward,
      count: count ?? this.count,
      onlyRent: onlyRent ?? this.onlyRent,
      group: group ?? this.group,
      firstElement: firstElement ?? this.firstElement,
    );
  }

  @override
  List<Object?> get props => [
        shortName,
        serviceName,
        keywords,
        price,
        forward,
        count,
        sellTop,
        freePrice,
        group,
      ];
}
