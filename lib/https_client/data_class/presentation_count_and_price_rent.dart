
class RentalTimePeriod{
  final int typeTime;
  final int timeRent;
  final int count;
  final double price;
  final String retailPrice;
  final bool freePrice;
  final bool isRent;
  final bool verificationByNumber;


  const RentalTimePeriod(
      {required this.typeTime,
        required this.timeRent,
        required this.count,
        required this.price,
        required this.retailPrice,
        this.freePrice = false,
        this.isRent = false,
        this.verificationByNumber = false,
      });

  RentalTimePeriod copyWith({
    int? typeTime,
    int? timeRent,
    int? count,
    double? price,
    String? retailPrice,
    bool? freePrice,
    bool? isRent,
    bool? verificationByNumber,
  }) {
    return RentalTimePeriod(
      typeTime: typeTime ?? this.typeTime,
      timeRent: timeRent ?? this.timeRent,
      count: count ?? this.count,
      price: price ?? this.price,
      retailPrice: retailPrice ?? this.retailPrice,
      freePrice: freePrice ?? this.freePrice,
      isRent: isRent ?? this.isRent,
      verificationByNumber: verificationByNumber ?? this.verificationByNumber,
    );
  }
}
