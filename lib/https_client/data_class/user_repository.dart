// import 'dart:convert';
//
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:sms_activate/data/sharedPreferences/shared_preferences_data.dart';
// import 'package:sms_activate/data/httpAdapter/modelsPresentation/presentation_user.dart';
// import 'package:sms_activate/dependencyInjection/injector.dart';
//
// class UserRepository {
//   final SharedPreferencesData spData;
//
//   static const secret = "secret_key";
//   static UserRepository? _instance;
//
//   factory UserRepository.getInstance() => _instance ??=
//       UserRepository._internal(SharedPreferencesData.getInstance());
//
//   UserRepository._internal(this.spData);
//
//   Future<bool> setUserSecret(final String secret) async {
//     final sp = locator<SharedPreferences>();
//     final result = await sp.setString(secret, secret);
//     return result;
//   }
//
//   Future<String> getUserSecret() async {
//     final sp = locator<SharedPreferences>();
//     return sp.getString(secret)??"";
//   }
//
//   Future<PresentationUser?> getUserData() async {
//     final String? userData =
//         await spData.getJson(SharedPreferencesData.keyUser);
//     if (userData != null) {
//       final Map<String,dynamic> converted = json.decode(userData);
//       final PresentationUser user = PresentationUser(
//           id: converted['id'],
//           refreshToken: converted['apikey'],
//           phoneForward: converted['phoneForward']??"",
//           sessionId: converted['sessionId'],
//           balance: converted['balance'],
//           cashback:converted['cashback'],
//           rank: converted['rank'],
//           email: converted['email'],
//           telegramId: converted['telegramId']??"",
//           refCode: converted['refCode']??"",
//           accountCreationDate: converted['accountCreationDate']??"",
//       );
//       return user;
//     }
//     else {
//       return null;
//     }
//   }
// }
