import 'package:equatable/equatable.dart';

class PresentationUser extends Equatable {
  final int id;
  final String refreshToken;
  final String sessionId;
  final double balance;
  final double cashback;
  final int rank;
  final String email;
  final String phoneForward;
  final String telegramId;
  final String accountCreationDate;
  final String refCode;

  const PresentationUser(
      {required this.id,
      required this.phoneForward,
      required this.refreshToken,
      required this.sessionId,
      required this.balance,
      required this.cashback,
      required this.rank,
      required this.email,
      this.telegramId = "",
      this.refCode = "",
      required this.accountCreationDate,
      });

  PresentationUser copyWith(
  {int? id,
     String? refreshToken,
     String? sessionId,
     double? balance,
     double? cashback,
     int? rank,
     String? email,
     String? phoneForward,
     String? telegramId,
     String? refCode,
     String? accountCreationDate,}
  ) {
    return PresentationUser(
      id: id ?? this.id,
      phoneForward: phoneForward ?? this.phoneForward,
      refreshToken: refreshToken ?? this.refreshToken,
      sessionId: sessionId ?? this.sessionId,
      balance: balance ?? this.balance,
      cashback: cashback ?? this.cashback,
      rank: rank ?? this.rank,
      email: email ?? this.email,
      telegramId: telegramId ?? this.telegramId,
      refCode: refCode ?? this.refCode,
      accountCreationDate: accountCreationDate ?? this.accountCreationDate,
    );
  }

  @override
  List<Object?> get props => [
        id,
        refreshToken,
        sessionId,
        balance,
        cashback,
        rank,
        email,
        phoneForward,
        telegramId,
        refCode,
        accountCreationDate
      ];

  Map<String, dynamic> toJson() => <String, dynamic>{
        "id": id,
        "apikey": refreshToken,
        "phoneForward": phoneForward,
        "sessionId": sessionId,
        "balance": balance,
        "cashback": cashback,
        "rank": rank,
        "email": email,
        "telegramId": telegramId,
        "refCode": refCode,
        "accountCreationDate": accountCreationDate
      };
}
