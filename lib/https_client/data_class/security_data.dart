class SecurityData {
  final String refreshToken;
  final String privateSession;
  final String sessionId;
  final String userid;
  final String telegramId;
  final String email;
  final String rank;
  final int bot;
  final bool whiteLabel;
  final bool telegramApi;

  const SecurityData({
    this.refreshToken = "",
    this.privateSession = "",
    this.sessionId = "",
    this.userid = "",
    this.telegramId = "",
    this.bot = 0,
    this.whiteLabel = false,
    this.telegramApi = true,
    this.email = "",
    this.rank = "",
  });

  SecurityData copyWith({
    String? refreshToken,
    String? sessionId,
    String? privateSession,
    String? userid,
    String? telegramId,
    String? email,
    String? rank,
    int? bot,
    bool? whiteLabel,
    bool? telegramApi,
  }) {
    return SecurityData(
      refreshToken: refreshToken ?? this.refreshToken,
      sessionId: sessionId ?? this.sessionId,
      privateSession: privateSession ?? this.privateSession,
      userid: userid ?? this.userid,
      telegramId: telegramId ?? this.telegramId,
      email: email ?? this.email,
      rank: rank ?? this.rank,
      bot: bot ?? this.bot,
      whiteLabel: whiteLabel ?? this.whiteLabel,
      telegramApi: telegramApi ?? this.telegramApi,
    );
  }
}
