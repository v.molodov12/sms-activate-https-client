import 'package:sms_activate_https_client/https_client/dto_models_https/countries/server_country.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/operators/server_operator.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/services/server_service.dart';

class AllServicesAndCountries {
  Map<String, dynamic>  _data = <String, dynamic>{};
  List<ServerService> listService = [];
  List<ServerCountry> listCountry = [];
  List<ServerOperator> listOperator = [];

   AllServicesAndCountries(dynamic httpData) {
    _data = httpData as Map<String, dynamic>;
    listService = getAllService();
    listCountry = getAllCountry();
    listOperator = getAllOperator();
    listOperator.add(getAnyOperator());
  }

   List<ServerService> getAllService() {
     final List<ServerService> servicesList = [];
     _data["services"].forEach((String shortName, service) {
       final splitName = shortName.split("_");
       final forward = splitName[1];
       final shortNameWithoutForward = splitName[0];
       service['forward'] = forward;
       service['short_name'] = shortNameWithoutForward;
       servicesList.add(ServerService.fromJson(service));
     });
     return servicesList;
   }

   List<ServerCountry> getAllCountry() {
     final List<ServerCountry> countriesList = [];
     _data["countries"].forEach((id, country) {
       country['id'] = id;
       countriesList.add(ServerCountry.fromJson(country));
     });
     return countriesList;
   }

   List<ServerOperator> getAllOperator() {
     final List<ServerOperator> operatorList = [];
     _data["operators"].forEach((idCountry, operatorsMap) {
       operatorsMap.forEach((key, value) {
         value['idCountry'] = idCountry;
         value['shortName'] = key;
         operatorList.add(ServerOperator.fromJson(value));
       });
     });
     return operatorList;
   }

  ServerOperator getAnyOperator() {
    final List<ServerOperator> operatorList = [];
    _data["any_operator"].forEach((idCountry, operators) {
      operators['idCountry'] = "";
      operators['shortName'] = idCountry;
      operatorList.add(ServerOperator.fromJson(operators));
    });
    return operatorList.first;
  }
}
