class UserIdDC {
  final String telegramId;
  final String email;

  const UserIdDC({
    required this.telegramId,
    required this.email,
  });
}
