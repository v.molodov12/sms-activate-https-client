import 'package:equatable/equatable.dart';

class PresentationCountry extends Equatable {
  final int id;
  final int count;
  final double price;
  final double retailPrice;
  final double? verifiedPrice;
  final int verifiedCount;
  final double? retailVerifiedPrice;
  final int verifiedCallCount;
  final String name;
  final bool isRent;
  final bool isFavorite;
  final Map<String, String> freePriceMap;

  const PresentationCountry({
    required this.id,
    required this.count,
    required this.price,
    required this.retailPrice,
    this.verifiedPrice,
    this.verifiedCount = 0,
    this.retailVerifiedPrice,
    this.verifiedCallCount = 0,
    required this.name,
    required this.freePriceMap,
    this.isRent = false,
    this.isFavorite = false,
  });

  @override
  List<Object?> get props => [
        id,
        count,
        price,
        retailPrice,
        name,
        freePriceMap,
        verifiedPrice,
        verifiedCount,
        verifiedCallCount,
        retailVerifiedPrice
      ];
}
