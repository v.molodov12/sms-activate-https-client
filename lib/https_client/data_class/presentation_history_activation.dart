import 'package:equatable/equatable.dart';

class PresentationHistoryActivation extends Equatable {
  final String service;
  final String createDate;
  final String country;
  final String id;
  final String serviceName;
  final String code2;
  final String code;
  final String cost;
  final String phone;
  final String status;
  final String forward;
  final bool isRent;

  const PresentationHistoryActivation(
      {required this.service,
      required this.serviceName,
      required this.createDate,
      required this.country,
      required this.id,
      required this.code2,
      required this.code,
      required this.cost,
      required this.phone,
      required this.status,
      required this.forward,
      this.isRent = false,
      });

  @override
  List<Object?> get props => [
        service,
        country,
        createDate,
        code,
        code2,
        cost,
        id,
        phone,
        status,
        forward,
      ];
}
