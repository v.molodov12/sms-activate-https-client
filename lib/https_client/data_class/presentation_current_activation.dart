import 'package:equatable/equatable.dart';

class PresentationCurrentActivation extends Equatable {
  final String id;
  final String forward;
  final String phone;
  final String service;
  final String countryId;
  final String serviceName;
  final String finishTimer;
  final String forwardNumber;
  final List<dynamic> messages;
  final String? message;
  final bool additionalActivationStatus;
  final String status;
  final String sortTime;

  const PresentationCurrentActivation(
      {required this.id,
      required this.messages,
      required this.forward,
      required this.status,
      required this.additionalActivationStatus,
      required this.message,
      required this.phone,
      required this.service,
      required this.countryId,
      required this.serviceName,
      required this.finishTimer,
      required this.forwardNumber,
      required this.sortTime});

  @override
  List<Object?> get props => [
        id,
        forward,
        phone,
        service,
        countryId,
        serviceName,
        messages,
        forwardNumber,
        finishTimer,
        message,
        status,
        additionalActivationStatus,
        sortTime
      ];
}
