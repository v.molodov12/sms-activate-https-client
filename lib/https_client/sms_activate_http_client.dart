import 'dart:async';
import 'dart:core';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/ddos_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/internet_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/Interceptors/default_error_interceptor.dart';
import 'package:sms_activate_https_client/https_client/data_class/security_data.dart';
import 'package:sms_activate_https_client/https_client/service/security_service.dart';
import 'package:sms_activate_https_client/shared_preferences/shared_preferences_telegram_header.dart';

class SmsActivateHttpClient {
  Timer? ddosTimer;
  final Dio dioDefault = Dio();
  static int maxHttpRequest = 100;
  late SecurityService _securityService;
  String _language = "en";
  bool _usingTelegramRestApi = false;

  SmsActivateHttpClient._internal() {
    ddosTimer = Timer.periodic(const Duration(seconds: 10), (timer) {
      maxHttpRequest = 100;
    });

    _securityService = SecurityService();

    dioDefault.interceptors.add(
      DefaultErrorInterceptor(
        securityService: _securityService,
      ),
    );
  }

  static final SmsActivateHttpClient _singleton =
      SmsActivateHttpClient._internal();

  factory SmsActivateHttpClient() => _singleton;

  void setCallbackUpdate({
    required ValueChanged<bool> failedUpdate,
    required ValueChanged<SecurityData> successUpdate,
  }) {
    _securityService.setCallbackUpdate(
      failedUpdate: failedUpdate,
      successUpdate: successUpdate,
    );
  }

  Future<void> setSecurityData({
    required SecurityData newData,
  }) async {
    String userData = await SharedPreferencesUserData.getUserData();
    if (userData.isNotEmpty && userData != newData.userid) {
      await SharedPreferencesUserData.clear();
    }

    await SharedPreferencesUserData.saveUserData(newData.userid);

    _securityService.setSecurityData(newData: newData);
  }

  void setLanguage({
    required String newLanguage,
  }) {
    _language = newLanguage;
  }

  get language => _language;

  setUsingTelegramRestApi({
    required bool usingTelegramRestApi,
  }) {
    _usingTelegramRestApi = usingTelegramRestApi;
  }

  get usingTelegramRestApi => _usingTelegramRestApi;

  SecurityData getSecurityData() => _securityService.getSecurityData();

  Future<T> getDataDHS<T>({
    Map<String, Object>? queryParams,
    Object? postParams,
    dynamic postAndGetParams,
    required String uri,
    required T Function(dynamic data) builder,
    required String method,
    bool addUserParams = true,
    bool addTelegramApi = false,
  }) async {
    try {
      Response response;

      if (maxHttpRequest <= 0) {
        throw DDoSException();
      } else {
        maxHttpRequest -= 1;
      }

      switch (method) {
        case 'post':
          response = await dioDefault.post(
            uri,
            data: postParams ?? {},
            options: Options(
              headers: {
                "addUserOptions": addUserParams,
                "addTelegramApi": addTelegramApi,
              },
            ),
          );
          break;
        case 'postAndGet':
          response = await dioDefault.post(
            uri,
            queryParameters: queryParams ?? {},
            options: Options(headers: {
              "addUserOptions": addUserParams,
              "addTelegramApi": addTelegramApi,
            }),
            data: postAndGetParams,
          );
          break;
        case 'get':
        default:
          response = await dioDefault.get(
            uri,
            queryParameters: queryParams ?? {},
            options: Options(
              headers: {
                "addUserOptions": addUserParams,
                "addTelegramApi": addTelegramApi,
              },
            ),
          );
          break;
      }
      if (kDebugMode) {
        print(response.realUri);
      }
      switch (response.statusCode) {
        case 200:
          return builder(response.data);
        default:
          throw ServerException();
      }
    } on SocketException catch (_) {
      throw InternetException();
    } on DioError catch (_) {
      rethrow;
    }
  }
}
