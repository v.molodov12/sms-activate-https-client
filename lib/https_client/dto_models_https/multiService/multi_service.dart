import 'package:json_annotation/json_annotation.dart';

part 'multi_service.generate.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class MultiService {
  final String idMultiService;
  String country;
  List<String> services;
  final List<String> forwards;
  final String operator;
  @JsonKey(name:"quant")
  final int count;
  @JsonKey(name:"totalOpt")
  final double priceOpt;
  @JsonKey(name:"totalRetail")
  final double priceRetail;
  int? statusMultiService;

  MultiService({
    required this.idMultiService,
    required this.country,
    required this.services,
    required this.forwards,
    required this.operator,
    required this.count,
    required this.priceOpt,
    required this.priceRetail,
    required this.statusMultiService,
  });

  factory MultiService.fromJson(final Map<String, dynamic> json, String idMultiService) =>
      _$MultiServiceFromJson(json,idMultiService);

  Map<String, dynamic> toJson() => _$MultiServiceToJson(this);

  @override
  List<Object?> get props => [
        idMultiService,
        country,
        services,
        forwards,
        operator,
        count,
        priceOpt,
        priceRetail,
        statusMultiService,
      ];
}
