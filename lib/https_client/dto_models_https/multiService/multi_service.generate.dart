part of 'multi_service.dart';

MultiService _$MultiServiceFromJson(
        Map<String, dynamic> json, String idMultiService) =>
    MultiService(
      idMultiService: idMultiService,
      country: json['country'] as String,
      services: _getServices(json),
      forwards: _getForwards(json),
      operator: "any",
      count: json['quant'] as int,
      priceOpt: (json['totalOpt'] as num).toDouble(),
      priceRetail: (json['totalRetail'] as num).toDouble(),
      statusMultiService: 1,
    );

List<String> _getServices(Map<String, dynamic> json) {
  List<dynamic> services = json['services'];
  List<String> newListService = [];
  for (var element in services) {
    newListService.add(element);
  }
  return newListService;
}

List<String> _getForwards(Map<String, dynamic> json) {
  List<dynamic> services = json['forwards'];
  List<String> newForwardsList = [];
  for (var element in services) {
    newForwardsList.add(element);
  }
  return newForwardsList;
}

Map<String, dynamic> _$MultiServiceToJson(MultiService instance) =>
    <String, dynamic>{
      'idMultiService': instance.idMultiService,
      'country': instance.country,
      'service': instance.services,
      'forwards': instance.forwards,
      'operator': instance.operator,
      'count': instance.count,
      'priceOpt': instance.priceOpt,
      'priceRetail': instance.priceRetail,
      'statusMultiService': instance.statusMultiService,
    };
