part of 'banners_message.dart';

BannersMessage _$ServerUserFromJson(Map<dynamic, dynamic> json) => BannersMessage(
      imageUrl: json['image_url'] as String,
      actionUrl: json['action_url'] as String,
      text: json['text'] as String,
      description: json['description'] as String,
      buttonText: json['button_text'] as String,
    );

Map<String, dynamic> _$ServerUserToJson(BannersMessage instance) =>
    <String, dynamic>{
      'imageUrl': instance.imageUrl,
      'actionUrl': instance.actionUrl,
      'text': instance.text,
      'description': instance.description,
      'buttonText': instance.buttonText,
    };
