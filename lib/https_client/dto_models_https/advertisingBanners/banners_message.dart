import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'banners_message_generate.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class BannersMessage extends Equatable {

  @JsonKey(name: "image_url")
  final String imageUrl;
  @JsonKey(name: "action_url")
  final String actionUrl;
  final String text;
  final String description;
  @JsonKey(name: "button_text")
  final String buttonText;

  const BannersMessage({
    required this.imageUrl,
    required this.text,
    required this.description,
    required this.buttonText,
    required this.actionUrl,
  });


  factory BannersMessage.fromJson(final Map<dynamic, dynamic> json) => _$ServerUserFromJson(json);
  Map<String, dynamic> toJson() => _$ServerUserToJson(this);


  @override
  List<Object?> get props => [imageUrl,actionUrl,text,description,buttonText];
}