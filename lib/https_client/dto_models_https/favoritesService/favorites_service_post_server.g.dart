// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorites_service_post_server.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavoritesServicePostServer _$FavoritesServicePostServerFromJson(
        Map<String, dynamic> json) =>
    FavoritesServicePostServer(
      country: json['country'] as String,
      service: json['service'] as String,
      forward: json['forward'] as String,
      operator: json['operator'] as String,
    );

Map<String, dynamic> _$FavoritesServicePostServerToJson(
        FavoritesServicePostServer instance) =>
    <String, dynamic>{
      'country': instance.country,
      'service': instance.service,
      'forward': instance.forward,
      'operator': instance.operator,
    };
