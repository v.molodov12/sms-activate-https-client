import 'package:json_annotation/json_annotation.dart';

part 'favorites_service.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class FavoritesService {
  final String id;
  final String country;
  final String service;
  final String forward;
  final String type;
  final String operator;
  final int count;
  final double price;
  @JsonKey(name: "retail_price")
  final double retailPrice;

  const FavoritesService({
    required this.id,
    required this.country,
    required this.service,
    required this.forward,
    required this.type,
    required this.operator,
    required this.count,
    required this.price,
    required this.retailPrice,
  });

  factory FavoritesService.fromJson(final Map<String, dynamic> json) =>
      _$FavoritesServiceFromJson(json);

  Map<String, dynamic> toJson() => _$FavoritesServiceToJson(this);

  @override
  List<Object?> get props => [
        id,
        country,
        service,
        forward,
        type,
        operator,
        count,
        price,
      ];
}
