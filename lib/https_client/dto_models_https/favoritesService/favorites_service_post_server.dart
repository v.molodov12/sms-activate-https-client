import 'package:json_annotation/json_annotation.dart';

part 'favorites_service_post_server.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class FavoritesServicePostServer {
  final String country;
  final String service;
  final String forward;
  final String operator;

  const FavoritesServicePostServer({
    required this.country,
    required this.service,
    required this.forward,
    required this.operator,
  });

  factory FavoritesServicePostServer.fromJson(final Map<String, dynamic> json) =>
      _$FavoritesServicePostServerFromJson(json);

  Map<String, dynamic> toJson() => _$FavoritesServicePostServerToJson(this);

  @override
  List<Object?> get props => [
        country,
        service,
        forward,
        operator,
      ];
}
