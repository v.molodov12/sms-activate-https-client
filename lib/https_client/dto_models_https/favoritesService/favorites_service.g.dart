// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'favorites_service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

FavoritesService _$FavoritesServiceFromJson(Map<String, dynamic> json) =>
    FavoritesService(
      id: json['id'] as String,
      country: json['country'] as String,
      service: json['service'] as String,
      forward: json['forward'] as String,
      type: json['type'] as String,
      operator: json['operator'] as String,
      count: json['count'] as int,
      price: (json['price'] as num).toDouble(),
      retailPrice: (json['retail_price'] as num).toDouble(),
    );

Map<String, dynamic> _$FavoritesServiceToJson(FavoritesService instance) =>
    <String, dynamic>{
      'id': instance.id,
      'country': instance.country,
      'service': instance.service,
      'forward': instance.forward,
      'type': instance.type,
      'operator': instance.operator,
      'count': instance.count,
      'price': instance.price,
      'retail_price': instance.retailPrice,
    };
