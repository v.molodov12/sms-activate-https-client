import 'package:collection/collection.dart';
import 'dart:convert';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'news_banner_generate.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class NewsBanner {
  @JsonKey(name: "id_banner")
  final int idBanner;
  @JsonKey(name: "version")
  final int version;
  @JsonKey(name: "image_url")
  final String imageUrl;
  final Map<String, String> text;
  final Map<String, String> description;
  @JsonKey(name: "button_text")
  final Map<String, String> buttonText;
  @JsonKey(name: "action")
  final Map<String, String> action;
  @JsonKey(name: "action_url")
  final String actionUrl;
  @JsonKey(name: "button_action")
  final String buttonAction;

  static NewsBanner? fromJson(final Map<String, dynamic> json) =>
      _$NewsBannerFromJson(json);

  const NewsBanner({
    required this.idBanner,
    required this.version,
    required this.imageUrl,
    required this.text,
    required this.description,
    required this.buttonText,
    required this.action,
    this.actionUrl = "",
    this.buttonAction = "",
  });

  Map<String, dynamic> toJson() => _$NewsBannerToJson(this);

  @override
  List<Object?> get props => [
        idBanner,
        version,
        imageUrl,
        text,
        description,
        buttonText,
        action,
      ];

  NewsBanner copyWith({
    int? idBanner,
    int? version,
    String? imageUrl,
    Map<String, String>? text,
    Map<String, String>? description,
    Map<String, String>? buttonText,
    Map<String, String>? action,
    String? actionUrl,
    String? buttonAction,
  }) =>
      NewsBanner(
        idBanner: idBanner ?? this.idBanner,
        version: version ?? this.version,
        imageUrl: imageUrl ?? this.imageUrl,
        text: text ?? this.text,
        description: description ?? this.description,
        buttonText: buttonText ?? this.buttonText,
        action: action ?? this.action,
        actionUrl: actionUrl ?? this.actionUrl,
        buttonAction: buttonAction ?? this.buttonAction,
      );

  static const _equality = MapEquality();
  @override
  bool operator ==(Object other) =>
      identical(this, other) || other is NewsBanner && runtimeType == other.runtimeType && idBanner == other.idBanner
          && _equality.equals(text,other.text)&& _equality.equals(description,other.description)&& version == other.version
          && _equality.equals(buttonText, other.buttonText)&& _equality.equals(action, other.action);

  @override
  int get hashCode => idBanner.hashCode;
}
