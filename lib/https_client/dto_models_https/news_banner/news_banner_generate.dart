part of 'news_banner.dart';

NewsBanner? _$NewsBannerFromJson(Map<String, dynamic> json) {
  if (json['version'] != null && json['version'] != 0||json['version_banner'] != null && json['version_banner'] != 0 ) {
    return NewsBanner(
      idBanner: json['id_banner'],
      version: json['version'] ?? json['version_banner']??"",
      imageUrl: json['image_url'].toString(),
      text: _getMap(json['text']),
      description: _getMap(json['description']),
      buttonText: _getMap(json['button_text']),
      action: _getMap(json['action']),
      actionUrl:
          json['action_url'] == null ? "" : json['action_url'].toString(),
      buttonAction:
          json['button_action'] == null ? "" : json['button_action'].toString(),
    );
  }
  return null;
}

Map<String, dynamic> _$NewsBannerToJson(NewsBanner instance,) =>
    <String, dynamic>{
      'id_banner':instance.idBanner,
      'version_banner': instance.version,
      'text': _getServerString(instance.text),
      'description':_getServerString(instance.description),
      'image_url': instance.imageUrl,
      'button_text': _getServerString(instance.buttonText),
      'action': _getServerString(instance.action),
      'action_url': instance.actionUrl,
      'button_action': instance.buttonAction,
    };

Map<String, String> _getMap(dynamic jsonInfo) {
  Map<String, String> newMap = {};
  dynamic newJsonInfo;
  if (jsonInfo is String) {
    String newString = jsonInfo.replaceAll('*', '"');
    String newStringTwo = newString.replaceAll('\n', r'\n');
    newJsonInfo = json.decode(newStringTwo);
  } else {
    newJsonInfo = jsonInfo;
  }
  if (newJsonInfo is Map<String, dynamic>) {
    newJsonInfo.forEach((key, value) {
      newMap[key] = value.toString();
    });
  }
  return newMap;
}

String _getServerString(Map<String, String> json) {
  String serverString =
      "{${json.entries.map((entry) => '*${entry.key}* : "${entry.value}"').join(',')}}";
  return serverString;
}
