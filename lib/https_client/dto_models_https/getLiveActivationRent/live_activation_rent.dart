import 'package:json_annotation/json_annotation.dart';

part 'live_activation_rent.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class LiveActivationRent {
  final String id;
  final String phone;
  final String service;
  final String country;
  final String createDate;
  @JsonKey(name: "estDate")
  final String finishDate;
  @JsonKey(name: "text")
  final String? sms;
  final String? status;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;

  const LiveActivationRent(
      {this.sms,
      required this.status,
      required this.id,
      required this.phone,
      required this.service,
      required this.country,
      required this.createDate,
      required this.finishDate,
      required this.lockCancelTime,
      });

  factory LiveActivationRent.fromJson(final Map<String, dynamic> json) => _$LiveActivationRentFromJson(json);

  Map<String, dynamic> toJson() => _$LiveActivationRentToJson(this);

  List<Object?> get props => [
        id,
        phone,
        service,
        country,
        createDate,
        finishDate,
        lockCancelTime,
      ];
}
