// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'live_activation_rent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LiveActivationRent _$LiveActivationRentFromJson(Map<String, dynamic> json) =>
    LiveActivationRent(
      sms: json['text'] as String?,
      status: json['status'] as String?,
      id: json['id'] as String,
      phone: json['phone'] as String,
      service: json['service'] as String,
      country: json['country'] as String,
      createDate: json['createDate'] as String,
      finishDate: json['estDate'] as String,
      lockCancelTime: json['lock_cancel_time'] as int,
    );

Map<String, dynamic> _$LiveActivationRentToJson(LiveActivationRent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'service': instance.service,
      'country': instance.country,
      'createDate': instance.createDate,
      'estDate': instance.finishDate,
      'text': instance.sms,
      'status': instance.status,
      'lock_cancel_time': instance.lockCancelTime,
    };
