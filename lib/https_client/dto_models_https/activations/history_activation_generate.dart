// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'history_activation.dart';

HistoryActivation _$HistoryActivationFromJson(Map<String, dynamic> json,bool existNext,int typeLoad) {
  if (json['code2'] != null) {
    try {
      return HistoryActivation(
        service: json['service'] as String,
        createDate: json['createDate'] as String,
        country: json['country'] as String,
        id: json['id'] as String,
        code2: json['code2'] as String,
        cost: json['cost'] as String,
        code: json['code'] == null
            ? ""
            : json['code'].toString(),
        phone: json['phone'] as String,
        status: json['status'] as String,
        forward: json['forward'] == null ? "0" : json['forward'] as String,
        isRent: json['isRent'] as bool,
        existNext: existNext,
        typeLoad: typeLoad,
      );
    } catch (ex) {
      return HistoryActivation(
        service: json['service'].toString(),
        createDate: json['createDate'].toString(),
        country: json['country'].toString(),
        id: json['id'].toString(),
        code2: "",
        cost: json['cost'].toString(),
        code: json['code'] == null
            ? ""
            : json['code'].toString(),
        phone: json['phone'].toString(),
        status: json['status'].toString(),
        forward: json['forward'] == null ? "0" : json['forward'] as String,
        isRent: json['isRent'] as bool,
        existNext: existNext,
        typeLoad: typeLoad,
      );
    }
  } else {
    String code = "";
    if(json['sms'] != null){
      Map<String,dynamic> mapSMS = json['sms'] as Map<String,dynamic>;
      code = mapSMS["0"] ?? "";
    }
    return HistoryActivation(
      service: json['service'].toString(),
      createDate: json['createDate'].toString(),
      country: json['country'].toString(),
      id: json['id'].toString(),
      code2: "",
      cost: json['cost'].toString(),
      code: json['code'] != null
          ? json['code'].toString()
          : code,
      phone: json['phone'].toString(),
      status: json['status'].toString(),
      forward: json['forward'] == null ? "0" : json['forward'] as String,
      isRent: json['isRent'] as bool,
      existNext: existNext,
      typeLoad: typeLoad,
    );
  }
}

HistoryActivation _$HistoryRentFromJson(
        Map<String, dynamic> json, bool existNext,int typeLoad) =>
    HistoryActivation(
      service: json['service'] as String,
      createDate: json['createDate'] as String,
      country: json['country'] as String,
      id: json['id'] as String,
      cost: json['cost'] as String,
      phone: json['phone'] as String,
      status: json['status'] as String,
      code: _getSms(json['sms']),
      code2: _getSms(json['sms']),
      forward: "0",
      isRent: true,
      existNext: existNext,
      typeLoad: typeLoad,
    );

String _getSms(Map<String, dynamic> sms){
  return sms[0] ?? "";
}

Map<String, dynamic> _$HistoryActivationToJson(HistoryActivation instance) =>
    <String, dynamic>{
      'service': instance.service,
      'createDate': instance.createDate,
      'country': instance.country,
      'cost': instance.cost,
      'id': instance.id,
      'code2': instance.code2,
      'code': instance.code,
      'phone': instance.phone,
      'status': instance.status,
      'forward': instance.forward,
      'isRent':instance.isRent,
      'existNext':instance.existNext,
      'typeLoad':instance.typeLoad,
    };
