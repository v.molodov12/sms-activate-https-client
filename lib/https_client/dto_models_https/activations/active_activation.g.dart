// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_activation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActiveActivation _$ActiveActivationFromJson(Map<String, dynamic> json) =>
    ActiveActivation(
      sms: json['sms'] as String?,
      status: json['status'] as String?,
      id: json['id'] as String,
      forward: json['forward'] as String,
      phone: json['phone'] as String,
      service: json['service'] as String,
      country: json['country'] as String,
      createDate: json['createDate'] as int,
      finishDate: json['finishDate'] as int,
      forwardNumber: json['forwardNumber'] as String,
    );

Map<String, dynamic> _$ActiveActivationToJson(ActiveActivation instance) =>
    <String, dynamic>{
      'id': instance.id,
      'forward': instance.forward,
      'phone': instance.phone,
      'service': instance.service,
      'country': instance.country,
      'createDate': instance.createDate,
      'finishDate': instance.finishDate,
      'sms': instance.sms,
      'status': instance.status,
      'forwardNumber': instance.forwardNumber,
    };
