import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'active_activation.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ActiveActivation extends Equatable {
  final String id;
  final String forward;
  final String phone;
  final String service;
  final String country;
  final int createDate;
  final int finishDate;
  final String? sms;
  final String? status;
  final String forwardNumber;

  const ActiveActivation({
    this.sms,
    required this.status,
    required this.id,
    required this.forward,
    required this.phone,
    required this.service,
    required this.country,
    required this.createDate,
    required this.finishDate,
    required this.forwardNumber,
  });

  factory ActiveActivation.fromJson(final Map<String, dynamic> json) =>
      _$ActiveActivationFromJson(json);

  Map<String, dynamic> toJson() => _$ActiveActivationToJson(this);

  @override
  List<Object?> get props => [
        id,
        forward,
        phone,
        service,
        country,
        createDate,
        finishDate,
        forwardNumber
      ];
}
