import 'package:equatable/equatable.dart';

part 'history_activation_generate.dart';
class HistoryActivation extends Equatable {

  final String service;
  final String createDate;
  final String country;
  final String cost;
  final String id;
  final String code2;
  final String code;
  final String phone;
  final String status;
  final String forward;
  final bool isRent;
  final bool existNext;
  final int typeLoad;

   const HistoryActivation(
      {required this.service,
        required this.createDate,
        required this.country,
        required this.id,
        required this.code2,
        required this.cost,
        required this.code,
        required this.phone,
        required this.status,
        required this.forward,
        required this.isRent,
        this.existNext = false,
        this.typeLoad = 1,
      });

  @override
  List<Object?> get props => [
    service,
    country,
    createDate,
    code,
    code2,
    id,
    cost,
    phone,
    status,
    forward,
    isRent,
    existNext,
    typeLoad,
  ];

  factory HistoryActivation.fromJson(final Map<String, dynamic> json,[bool existNext = false,int typeLoad = 1,]) => _$HistoryActivationFromJson(json,existNext,typeLoad);

  factory HistoryActivation.fromJsonRent(final Map<String, dynamic> json,[bool existNext = false,int typeLoad = 1,]) => _$HistoryRentFromJson(json,existNext,typeLoad);

  Map<String, dynamic> toJson() => _$HistoryActivationToJson(this);

}