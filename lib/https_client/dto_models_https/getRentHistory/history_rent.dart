import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'history_rent_gen.dart';
class HistoryRent extends Equatable {

  final String service;
  final String createDate;
  final String country;
  final String cost;
  final String id;
  @JsonKey(name:"sms")
  String? code;
  final String phone;
  final String status;
  final String forward;

  HistoryRent(
      {required this.service,
        required this.createDate,
        required this.country,
        required this.id,
        required this.cost,
        required this.phone,
        required this.status,
        required this.forward,
        this.code = ""});

  @override
  List<Object?> get props => [
    service,
    country,
    createDate,
    code,
    id,
    cost,
    phone,
    status,
    forward,
  ];

  factory HistoryRent.fromJson(final Map<String, dynamic> json) => _$HistoryRentFromJson(json);

  HistoryRent setCode(Map<String, dynamic> sms){
    if (sms.isNotEmpty) {
      code = sms["0"];
    } else {
      code = "";
    }
    return this;
  }

  Map<String, dynamic> toJson() => _$HistoryRentToJson(this);
}
