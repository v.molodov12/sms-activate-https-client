part of 'history_rent.dart';
HistoryRent _$HistoryRentFromJson(Map<String, dynamic> json) => HistoryRent(
      service: json['service'] as String,
      createDate: json['createDate'] as String,
      country: json['country'] as String,
      id: json['id'] as String,
      cost: json['cost'] as String,
      phone: json['phone'] as String,
      status: json['status'] as String,
      forward: "0",
    );

Map<String, dynamic> _$HistoryRentToJson(HistoryRent instance) =>
    <String, dynamic>{
      'service': instance.service,
      'createDate': instance.createDate,
      'country': instance.country,
      'cost': instance.cost,
      'id': instance.id,
      'phone': instance.phone,
      'status': instance.status,
      'forward': instance.forward,
    };
