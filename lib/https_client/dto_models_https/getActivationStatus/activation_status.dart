import 'package:json_annotation/json_annotation.dart';

// part 'activation_status.g.dart';
//
// @JsonSerializable(fieldRename: FieldRename.none)
// class ActivationRentStatus {
//   @JsonKey(name: "phoneFrom")
//   final String phone;
//   @JsonKey(name: "text")
//   final String textSms;
//   @JsonKey(name: "service")
//   final String serviceName;
//   @JsonKey(name: "date")
//   final String date;
//
//   const ActivationRentStatus(
//       {required this.phone,
//         required this.textSms,
//         required this.serviceName,
//         required this.date,});
//
//   factory ActivationRentStatus.fromJson(final Map<String, dynamic> json) => _$ActivationRentStatusFromJson(json);
//
//   Map<String, dynamic> toJson() => _$ActivationRentStatusToJson(this);
//
//   @override
//   List<Object?> get props => [
//     phone,
//     textSms,
//     serviceName,
//     date
//   ];
// }
