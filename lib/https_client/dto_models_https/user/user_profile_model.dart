import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'user_profile_model_generate.dart';

class ServerUser extends Equatable {
  final String sessionId;
  final String balance;
  final String cashback;
  @JsonKey(name: "refresh_token")
  final String refreshToken;
  @JsonKey(name: "userid")
  final int id;
  final String email;
  @JsonKey(name: "telegram_id")
  final String telegramId;
  final String accountCreationDate;

  const ServerUser({
    required this.sessionId,
    required this.balance,
    required this.cashback,
    required this.refreshToken,
    required this.id,
    required this.email,
    required this.telegramId,
    required this.accountCreationDate,
  });

  factory ServerUser.fromJson(final Map<String, dynamic> json) =>
      _$ServerUserFromJson(json);

  Map<String, dynamic> toJson() => _$ServerUserToJson(this);

  @override
  List<Object?> get props => [id, email, balance, refreshToken,sessionId,cashback];
}
