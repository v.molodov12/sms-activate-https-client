
part of 'user_profile_model.dart';

ServerUser _$ServerUserFromJson(Map<String, dynamic> json) => ServerUser(
      sessionId: json['sessionId'] as String,
      balance: json['balance'] as String,
      cashback: json['cashback'] as String,
      refreshToken: json['refresh_token'] as String,
      id: int.parse(json['userid'].toString()),
      email: json['email'] != null ? json['email'] as String : "",
      telegramId: json['telegram_id'] != null ? json['telegram_id'] as String : "",
      accountCreationDate: json['accountCreationDate'].toString(),
    );

Map<String, dynamic> _$ServerUserToJson(ServerUser instance) =>
    <String, dynamic>{
      'sessionId': instance.sessionId,
      'balance': instance.balance,
      'cashback': instance.cashback,
      'refresh_token': instance.refreshToken,
      'userid': instance.id,
      'email': instance.email,
      'telegramId': instance.telegramId,
      'accountCreationDate': instance.accountCreationDate,
    };
