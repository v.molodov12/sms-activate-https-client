part of 'server_top_country.dart';

ServerTopCountry _$ServerTopCountryFromJson(
  Map<String, dynamic> json,
  final int country,
  int userRank,
) {
  String retailPrice =
      json['retailPrice'] != null ? json['retailPrice'].toString() : "0";
  String defaultPrice =
      json['defaultPrice'] != null ? json['defaultPrice'].toString() : "0";
  if (json['isRent'] != null || json['rent'] != null) {
    String keyRent = "isRent";
    if (json['rent'] != null) {
      keyRent = "rent";
    }
    return ServerTopCountry(
      country: country,
      totalCount: json['totalCount'] ?? 0,
      defaultPrice: (json['defaultPrice'] as num).toDouble(),
      retailPrice: (json['retailPrice'] as num).toDouble(),
      verifiedPrice: (json['verifiedPrice'] as num).toDouble(),
      verifiedCount: json['verifiedCount'],
      verifiedCallCount: json['verifiedCallCount'],
      retailVerifiedPrice: (json['retailVerifiedPrice'] as num).toDouble(),
      isRent: (json[keyRent] as int) == 1,
      freePriceMap: json['priceMap'] != null
          ? _getFreePriceMap(
              json['priceMap'],
              userRank,
              retailPrice,
              defaultPrice,
            )
          : {},
      index: json['index'] ?? 0,
    );
  } else {
    return ServerTopCountry(
      country: country,
      totalCount: json['totalCount'] ?? 0,
      defaultPrice: (json['defaultPrice'] as num).toDouble(),
      retailPrice: (json['retailPrice'] as num).toDouble(),
      verifiedPrice: (json['verifiedPrice'] as num).toDouble(),
      verifiedCount: json['verifiedCount'],
      verifiedCallCount: json['verifiedCallCount'],
      retailVerifiedPrice: (json['retailVerifiedPrice'] as num).toDouble(),
      isRent: true,
      freePriceMap: json['priceMap'] != null
          ? _getFreePriceMap(
              json['priceMap'],
              userRank,
              retailPrice,
              defaultPrice,
            )
          : {},
      index: json['index'] ?? 0,
    );
  }
}

Map<String, String> _getFreePriceMap(
  Map<dynamic, dynamic> castMap,
  int userRank,
  String retailPrice,
  String optPrice,
) {
  Map<String, String> result = {};
  try {
    if (userRank == 0) {
      double retailPriceD = double.tryParse(retailPrice) ?? 0;
      for (final element in castMap.entries) {
        double keyD = double.tryParse(element.key) ?? 0;
        if (keyD >= retailPriceD) {
          result[element.key] = element.value.toString();
        }
      }
    } else {
      double optPriceD = double.tryParse(optPrice) ?? 0;
      for (final element in castMap.entries) {
        double keyD = double.tryParse(element.key) ?? 0;
        if (keyD >= optPriceD) {
          result[element.key] = element.value.toString();
        }
      }
    }
    return result;
  } catch (ex) {
    throw ParsJsonException();
  }
}

Map<String, dynamic> _$ServerTopCountryToJson(ServerTopCountry instance) =>
    <String, dynamic>{
      'country': instance.country,
      'count': instance.totalCount,
      'defaultPrice': instance.defaultPrice,
      'retailPrice': instance.retailPrice,
      'verifiedPrice': instance.verifiedPrice,
      'verifiedCount': instance.verifiedCount,
      'isRent': instance.isRent,
      'freePriceMap': instance.freePriceMap,
      'index': instance.index,
    };
