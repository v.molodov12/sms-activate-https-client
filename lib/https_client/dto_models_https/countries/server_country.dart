import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_country_generate.dart';

class ServerCountry extends Equatable {

  final String ru;
  final String en;
  final String cn;
  final String id;
  final String? regionCode;
  final int ms;

  factory ServerCountry.fromJson(final Map<String, dynamic> json) => _$ServerCountryFromJson(json);

  const ServerCountry({required this.ru, required this.en, required this.cn, required this.id,required this.regionCode,required this.ms});

  Map<String, dynamic> toJson() => _$ServerCountryToJson(this);

  @override
  List<Object?> get props => [ru,en,cn,id,regionCode];

}