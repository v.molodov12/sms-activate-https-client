import 'package:equatable/equatable.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/pars_json_exception.dart';
part 'server_top_country_to_json.dart';

class ServerTopCountry extends Equatable {
  final int country;
  final int totalCount;
  final double retailPrice;
  final double verifiedPrice;
  final int verifiedCount;
  final double defaultPrice;
  final bool isRent;
  final double retailVerifiedPrice;
  final int verifiedCallCount;
  final Map<String, String> freePriceMap;
  final int index;

  factory ServerTopCountry.fromJson(
          final Map<String, dynamic> json, final int country, int userRank) =>
      _$ServerTopCountryFromJson(json, country, userRank);

  const ServerTopCountry({
    required this.country,
    required this.totalCount,
    required this.defaultPrice,
    required this.retailPrice,
    required this.isRent,
    required this.freePriceMap,
    required this.verifiedPrice,
    required this.verifiedCount,
    required this.retailVerifiedPrice,
    required this.verifiedCallCount,
    required this.index,
  });

  Map<String, dynamic> toJson() => _$ServerTopCountryToJson(this);

  @override
  List<Object?> get props => [
        country,
        totalCount,
        defaultPrice,
        isRent,
        verifiedCount,
        verifiedPrice,
        retailPrice,
        verifiedCallCount,
        retailVerifiedPrice,
        index,
      ];
}
