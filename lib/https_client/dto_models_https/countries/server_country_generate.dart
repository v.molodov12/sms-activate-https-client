part of 'server_country.dart';

ServerCountry _$ServerCountryFromJson(Map<String, dynamic> json) {
  try {
    return ServerCountry(
      ru: json['ru'] as String,
      en: json['en'] as String,
      cn: json['cn'] as String,
      id: json['id'] as String,
      regionCode: json['regionCode'] as String?,
      ms: json['ms'] as int,
    );
  } catch (ex) {
    print("invalid country format/неверный формат страны {метод:AllServicesAndCountry --> $json}");
  }

  return const ServerCountry(
    ru: "invalid country format",
    en: "invalid country format",
    cn: "invalid country format",
    id: "-1",
    regionCode: "invalid country format",
    ms: 0,
  );
}

Map<String, dynamic> _$ServerCountryToJson(ServerCountry instance) =>
    <String, dynamic>{
      'ru': instance.ru,
      'en': instance.en,
      'cn': instance.cn,
      'id': instance.id,
      'regionCode': instance.regionCode,
      'ms': instance.ms,
    };
