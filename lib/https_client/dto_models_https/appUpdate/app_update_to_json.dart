part of 'app_update.dart';

AppUpdate _$AppUpdateFromJson(Map<String, dynamic> json) => AppUpdate(
      marketId: int.tryParse(json['market_id'])??1,
      marketName: json['market_name'] as String,
      downloadCount: int.tryParse(json['download_count'])??0,
      appVersion: double.parse(json['app_version']),
      criticalVersion: double.parse(json['critical_version']),
    );

Map<String, dynamic> _$AppUpdateToJson(AppUpdate instance) => <String, dynamic>{
      'market_id': instance.marketId,
      'market_name': instance.marketName,
      'download_count': instance.downloadCount,
      'app_version': instance.appVersion,
      'critical_version': instance.criticalVersion,
    };
