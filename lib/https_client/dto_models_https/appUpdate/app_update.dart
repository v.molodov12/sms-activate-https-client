import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_update_to_json.dart';
class AppUpdate extends Equatable {
  @JsonKey(name:"market_id")
  final int marketId;
  @JsonKey(name:"market_name")
  final String marketName;
  @JsonKey(name:"download_count")
  final int downloadCount;
  @JsonKey(name:"app_version")
  final double appVersion;
  @JsonKey(name:"critical_version")
  final double criticalVersion;

  const AppUpdate(
      {required this.marketId,
        required this.marketName,
        required this.downloadCount,
        required this.appVersion,
        required this.criticalVersion,
       });

  @override
  List<Object?> get props => [
    marketId,
    marketName,
    downloadCount,
    appVersion,
    criticalVersion,
  ];

  factory AppUpdate.fromJson(final Map<String, dynamic> json) => _$AppUpdateFromJson(json);

  Map<String, dynamic> toJson() => _$AppUpdateToJson(this);
}
