part of 'number_activation_tg.dart';

NumberActivationTg _$NumberActivationTgFromJson(Map<String, dynamic> json) =>
    NumberActivationTg(
      id: int.tryParse(json['id'].toString()) ?? 0,
      number: json['phone'] as String,
      createDate: json['createDate'] as String,
      finishDate: json["activationEnd"] as String,
      lockCancelTime: json['lock_cancel_time'] != null
          ? json['lock_cancel_time'] as int
          : 120,
      forward: json['forward'] as String,
      allSms: json['moreSms'] as String,
      sms: json['sms'] as String,
      code: json['code'] as String,
      status: json['status_code'] as String,
      call: json['call'] as String,
    );

Map<String, dynamic> _$NumberActivationTgToJson(NumberActivationTg instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.number,
      'createDate': instance.createDate,
      'activationEnd': instance.finishDate,
      'lock_cancel_time': instance.lockCancelTime,
      'forward': instance.forward,
      'moreSms': instance.allSms,
      'sms': instance.sms,
      'code': instance.code,
      'status_code': instance.status,
      'call': instance.call,
    };
