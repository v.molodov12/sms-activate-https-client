// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'number_activation.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NumberActivation _$NumberActivationFromJson(Map<String, dynamic> json) =>
    NumberActivation(
      id: json['activation'] as String,
      phone: json['phone'] as String,
      timeStart: json['timeStart'] as String,
      timeEnd: json['timeEnd'] as String,
      lockCancelTime: json['lock_cancel_time'] as int,
    );

Map<String, dynamic> _$NumberActivationToJson(NumberActivation instance) =>
    <String, dynamic>{
      'activation': instance.id,
      'phone': instance.phone,
      'timeStart': instance.timeStart,
      'timeEnd': instance.timeEnd,
      'lock_cancel_time': instance.lockCancelTime,
    };
