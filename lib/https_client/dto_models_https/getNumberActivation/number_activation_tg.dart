import 'package:json_annotation/json_annotation.dart';

part 'number_activation_tg_generate.dart';
class NumberActivationTg{
  final int id;
  @JsonKey(name: "phone")
  final String number;
  final String createDate;
  @JsonKey(name: "activationEnd")
  final String finishDate;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;
  final String forward;
  @JsonKey(name: "moreSms")
  final String allSms;
  final String sms;
  final String code;
  @JsonKey(name: "status_code")
  final String status;
  final String call;

  const NumberActivationTg(
      {required  this.id,
        required this.number,
        required this.createDate,
        required this.finishDate,
        required this.lockCancelTime,
        required this.forward,
        required this.allSms,
        required this.sms,
        required this.code,
        required this.status,
        required this.call,
      });

  factory NumberActivationTg.fromJson(final Map<String, dynamic> json) => _$NumberActivationTgFromJson(json);

  Map<String, dynamic> toJson() => _$NumberActivationTgToJson(this);

  @override
  List<Object?> get props => [
    id,
    number,
    createDate,
    finishDate,
    lockCancelTime,
    forward,
    allSms,
    sms,
    code,
    status,
    call,
  ];
}
