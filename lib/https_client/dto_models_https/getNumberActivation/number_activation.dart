import 'package:json_annotation/json_annotation.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getNumberActivation/number_activation_tg.dart';

part 'number_activation.g.dart';
@JsonSerializable(fieldRename: FieldRename.none)
class NumberActivation{
  @JsonKey(name: "activation")
  final String id;
  final String phone;
  final String timeStart;
  final String timeEnd;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;

  const NumberActivation(
      {required this.id,
        required this.phone,
        required this.timeStart,
        required this.timeEnd,
        required this.lockCancelTime,
      });

  factory NumberActivation.fromJson(final Map<String, dynamic> json) => _$NumberActivationFromJson(json);

  Map<String, dynamic> toJson() => _$NumberActivationToJson(this);

  @override
  List<Object?> get props => [
    id,
    phone,
    timeStart,
    timeEnd,
    lockCancelTime,
  ];

  factory NumberActivation.adapter(NumberActivationTg numberInfo) {
    return NumberActivation(
      id: "${numberInfo.id}",
      phone: numberInfo.number,
      timeStart: numberInfo.createDate,
      timeEnd: numberInfo.finishDate,
      lockCancelTime: numberInfo.lockCancelTime,
    );
  }

}
