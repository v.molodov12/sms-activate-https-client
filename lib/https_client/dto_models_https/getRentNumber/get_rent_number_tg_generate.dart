part of 'get_rent_number_tg.dart';

GetRentNumberTg _$GetRentNumberTgFromJson(Map<String, dynamic> json) =>
    GetRentNumberTg(
      id: int.tryParse(json['id'].toString()) ?? 0,
      number: json['phone'] as String,
      finishDate: json['activationEnd'] as String,
      createDate: json['createDate'] as String,
          lockCancelTime: json['lock_cancel_time'] != null
              ? json['lock_cancel_time'] as int
              : 120,
      allSms: json['allSms'] as String,
      sms: json['sms'] as String,
      diff: json['diff'] as String,
      status: json['status_code'] as String,
      code: json['code'] as String,
      call: json['call'] as String,
    );

Map<String, dynamic> _$GetRentNumberTgToJson(GetRentNumberTg instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.number,
      'createDate': instance.createDate,
      'activationEnd': instance.finishDate,
      'lock_cancel_time': instance.lockCancelTime,
      'allSms': instance.allSms,
      'sms': instance.sms,
      'diff': instance.diff,
      'status_code': instance.status,
      'code': instance.code,
      'call': instance.call,
    };
