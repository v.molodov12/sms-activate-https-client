// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'get_rent_number.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GetRentNumber _$GetRentNumberFromJson(Map<String, dynamic> json) =>
    GetRentNumber(
      id: json['id'] as int,
      phone: json['number'] as String,
      endDate: json['endDate'] as String,
      lockCancelTime: json['lock_cancel_time'] as int,
    );

Map<String, dynamic> _$GetRentNumberToJson(GetRentNumber instance) =>
    <String, dynamic>{
      'id': instance.id,
      'phone': instance.phone,
      'endDate': instance.endDate,
      'lock_cancel_time': instance.lockCancelTime,
    };
