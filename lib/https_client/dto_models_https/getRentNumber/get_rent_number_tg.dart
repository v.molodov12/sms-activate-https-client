import 'package:json_annotation/json_annotation.dart';

part 'get_rent_number_tg_generate.dart';

class GetRentNumberTg {
  final int id;
  @JsonKey(name: "phone")
  final String number;
  final String createDate;
  @JsonKey(name: "activationEnd")
  final String finishDate;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;
  final String allSms;
  final String sms;
  final String diff;
  @JsonKey(name: "status_code")
  final String status;
  final String code;
  final String call;

  const GetRentNumberTg(
      {required this.id,
        required this.number,
        required this.finishDate,
        required this.createDate,
        required this.lockCancelTime,
        required this.allSms,
        required this.sms,
        required this.diff,
        required this.status,
        required this.code,
        required this.call,
      });

  factory GetRentNumberTg.fromJson(final Map<String, dynamic> json) => _$GetRentNumberTgFromJson(json);

  Map<String, dynamic> toJson() => _$GetRentNumberTgToJson(this);

  @override
  List<Object?> get props => [
    id,
    number,
    createDate,
    lockCancelTime,
    allSms,
    sms,
    diff,
    status,
    code,
    call,
  ];
}
