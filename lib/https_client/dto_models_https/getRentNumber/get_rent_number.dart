import 'package:json_annotation/json_annotation.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getRentNumber/get_rent_number_tg.dart';

part 'get_rent_number.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class GetRentNumber {
  final int id;
  @JsonKey(name: "number")
  final String phone;
  final String endDate;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;

  const GetRentNumber({
    required this.id,
    required this.phone,
    required this.endDate,
    required this.lockCancelTime,
  });

  factory GetRentNumber.fromJson(final Map<String, dynamic> json) =>
      _$GetRentNumberFromJson(json);

  Map<String, dynamic> toJson() => _$GetRentNumberToJson(this);

  factory GetRentNumber.adapter(GetRentNumberTg numberInfo) {
    return GetRentNumber(
      id: numberInfo.id,
      phone: numberInfo.number,
      endDate: numberInfo.finishDate,
      lockCancelTime: numberInfo.lockCancelTime,
    );
  }

  @override
  List<Object?> get props => [
        id,
        phone,
        endDate,
        lockCancelTime,
      ];
}
