// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'activation_rent_status.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActivationRentStatus _$ActivationRentStatusFromJson(
        Map<String, dynamic> json) =>
    ActivationRentStatus(
      phone: json['phoneFrom'] as String,
      textSms: json['text'] as String,
      serviceName: json['service'] as String,
      date: json['date'] as String,
    );

Map<String, dynamic> _$ActivationRentStatusToJson(
        ActivationRentStatus instance) =>
    <String, dynamic>{
      'phoneFrom': instance.phone,
      'text': instance.textSms,
      'service': instance.serviceName,
      'date': instance.date,
    };
