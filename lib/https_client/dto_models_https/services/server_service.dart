import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_service.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ServerService extends Equatable{

  @JsonKey(name: "short_name")
  final String shortName;
  @JsonKey(name: "ru")
  final String ruName;
  @JsonKey(name: "en")
  final String enName;
  final String? keywords;
  final String forward;

  const ServerService({required this.shortName, required this.ruName, required this.enName, this.keywords, required this.forward});


  factory ServerService.fromJson(final Map<String, dynamic> json) => _$ServerServiceFromJson(json);

  Map<String, dynamic> toJson() => _$ServerServiceToJson(this);


  @override
  List<Object?> get props => [shortName,ruName,enName,keywords,forward];

}