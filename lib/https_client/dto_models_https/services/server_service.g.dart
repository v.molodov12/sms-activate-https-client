// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_service.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerService _$ServerServiceFromJson(Map<String, dynamic> json) =>
    ServerService(
      shortName: json['short_name'] as String,
      ruName: json['ru'] as String,
      enName: json['en'] as String,
      keywords: json['keywords'] as String?,
      forward: json['forward'] as String,
    );

Map<String, dynamic> _$ServerServiceToJson(ServerService instance) =>
    <String, dynamic>{
      'short_name': instance.shortName,
      'ru': instance.ruName,
      'en': instance.enName,
      'keywords': instance.keywords,
      'forward': instance.forward,
    };
