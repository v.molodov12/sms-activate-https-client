import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_service_with_price_companion.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ServerServiceWithPrice extends Equatable {
  final String shortName;
  final int sellTop;
  final int totalCount;
  final double minPrice;
  final double minFreePrice;
  final String forward;
  final bool onlyRent;

  factory ServerServiceWithPrice.fromJson(final Map<String, dynamic> json) =>
      _$ServerServiceWithPriceFromJson(json);

  const ServerServiceWithPrice(
      {required this.forward,
      required this.shortName,
      required this.sellTop,
      required this.totalCount,
      required this.minPrice,
      required this.minFreePrice,
      required this.onlyRent});

  Map<String, dynamic> toJson() => _$ServerServiceWithPriceToJson(this);

  @override
  List<Object?> get props =>
      [shortName, sellTop, totalCount, minFreePrice, minPrice];
}
