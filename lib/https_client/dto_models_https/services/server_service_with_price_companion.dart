part of 'server_service_with_price.dart';

ServerServiceWithPrice _$ServerServiceWithPriceFromJson(
    Map<String, dynamic> json) {
  if (json['shortName'] as String == "full") {
    return ServerServiceWithPrice(
      forward: json['forward'] ? "1" : "0",
      shortName: json['shortName'] as String,
      sellTop: json['sellTop'] as int,
      totalCount: json['totalCount'] as int,
      minPrice:
          json['minPrice'] != null ? (json['minPrice'] as num).toDouble() : 0,
      minFreePrice: json['minFreePrice'] != null
          ? (json['minFreePrice'] as num).toDouble()
          : 0,
      onlyRent: json['onlyRent'] != null ? (json['onlyRent'] as bool) : false,
    );
  } else {
    return ServerServiceWithPrice(
      forward: json['forward'] ? "1" : "0",
      shortName: json['shortName'] as String,
      sellTop: json['sellTop'] as int,
      totalCount: json['totalCount'] as int,
      minPrice: (json['minPrice'] as num).toDouble(),
      minFreePrice: (json['minFreePrice'] as num).toDouble(),
      onlyRent: json['onlyRent'] != null ? (json['onlyRent'] as bool) : false,
    );
  }
}

Map<String, dynamic> _$ServerServiceWithPriceToJson(
        ServerServiceWithPrice instance) =>
    <String, dynamic>{
      'shortName': instance.shortName,
      'sellTop': instance.sellTop,
      'totalCount': instance.totalCount,
      'minPrice': instance.minPrice,
      'minFreePrice': instance.minFreePrice,
      'forward': instance.forward,
      'onlyRent': instance.onlyRent,
    };
