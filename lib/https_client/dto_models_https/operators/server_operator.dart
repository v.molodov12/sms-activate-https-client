import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_operator_generate.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ServerOperator extends Equatable {

  final String idCountry;
  final String shortName;
  final String ru;
  final String en;
  final String cn;


  factory ServerOperator.fromJson(final Map<String, dynamic> json) => _$ServerOperatorFromJson(json);

  const ServerOperator({required this.idCountry, required this.shortName, required this.ru, required this.en, required this.cn});

  Map<String, dynamic> toJson() => _$ServerOperatorToJson(this);

  @override
  List<Object?> get props => [idCountry,shortName,ru,en,cn];

}