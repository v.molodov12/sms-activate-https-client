part of 'server_operator.dart';

ServerOperator _$ServerOperatorFromJson(Map<String, dynamic> json) {
  if (json['shortName'] as String != "any") {
    return ServerOperator(
      idCountry: json['idCountry'] as String,
      shortName: json['shortName'] as String,
      ru: json['ru'] as String,
      en: json['en'] as String,
      cn: json['cn'] as String,
    );
  } else {
    return ServerOperator(
      idCountry: json['idCountry'] as String,
      shortName: json['shortName'] as String,
      ru: "Любой оператор",
      en: "Any operator",
      cn: "任何運營商",
    );
  }
}

Map<String, dynamic> _$ServerOperatorToJson(ServerOperator instance) =>
    <String, dynamic>{
      'idCountry': instance.idCountry,
      'shortName': instance.shortName,
      'ru': instance.ru,
      'en': instance.en,
      'cn': instance.cn,
    };
