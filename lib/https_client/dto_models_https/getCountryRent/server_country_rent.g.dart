// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'server_country_rent.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ServerCountryRent _$ServerCountryRentFromJson(Map<String, dynamic> json) =>
    ServerCountryRent(
      id: json['id'] as String,
      name: json['name'] as String,
    );

Map<String, dynamic> _$ServerCountryRentToJson(ServerCountryRent instance) =>
    <String, dynamic>{
      'id': instance.id,
      'name': instance.name,
    };
