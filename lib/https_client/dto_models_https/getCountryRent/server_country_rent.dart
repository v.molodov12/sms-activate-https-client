import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'server_country_rent.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ServerCountryRent extends Equatable {

  final String id;
  final String name;

  factory ServerCountryRent.fromJson(final Map<String, dynamic> json) => _$ServerCountryRentFromJson(json);

  const ServerCountryRent({required this.id, required this.name});

  Map<String, dynamic> toJson() => _$ServerCountryRentToJson(this);

  @override
  List<Object?> get props => [id,name];

}