part 'reactivation_generated.dart';

class Reactivation {
  final String msg;
  final String cost;
  final String service;
  final String phone;
  final String country;

  factory Reactivation.fromJson(final Map<String, dynamic> json) => _$ReactivationFromJson(json);

  const Reactivation({required this.msg, required this.cost, required this.service, required this.phone, required this.country});

  Map<String, dynamic> toJson() => _$ReactivationToJson(this);

  @override
  List<Object?> get props => [msg,cost,service,phone,country];
  // узнать доступна ли реактивация
  // https://sms-activate.org/stubs/handler_api.php?owner=5&action=checkExtraActivation&sessionId=bf59444e5735635cf5f9a6e58d124240&userid=1155497&activationId=1321103014
  // {"status":"success","msg":"Стоимость продления активации составит 300 ₽","cost":300,"service":"ot","phone":"79256033722","country":"0"}

  // получить номер для реактивации
  // https://sms-activate.org/stubs/handler_api.php?owner=5&action=getExtraActivation&sessionId=bf59444e5735635cf5f9a6e58d124240&userid=1155497&activationId=1321103014
  // {"status":"error","msg":"К сожалению, продлить данную активацию невозможно","error":"SIM_OFFLINE"}
// узнать доступна ли аренда
// https://sms-activate.org/stubs/handler_api.php?refresh_token=a12062a62ce1197a4fe629184634386a&sessionId=641357a9e59639d667a759e236b97900&owner=5&userid=1079031&rent_time=4&action=getContinueRentPriceNumber&id=9018730
//{"status":"success","price":13.8}
// {"status":"error","message":"RENT_DIE"}
// реактивировать аренду
// https://sms-activate.org/stubs/handler_api.php?owner=5&rent_time=4&action=continueRentNumber&sessionId=bf59444e5735635cf5f9a6e58d124240&id=8970985&userid=1155497
}