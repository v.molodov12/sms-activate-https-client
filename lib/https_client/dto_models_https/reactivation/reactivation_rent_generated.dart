part of 'reactivation_rent.dart';

ReactivationRent _$ReactivationRentFromJson(Map<String, dynamic> json) =>
    ReactivationRent(
      price: json['price'].toString(),
    );

Map<String, dynamic> _$ReactivationRentToJson(ReactivationRent instance) =>
    <String, dynamic>{
      'price': instance.price,
    };
