part 'reactivation_rent_generated.dart';

class ReactivationRent {
  final String price;

  factory ReactivationRent.fromJson(final Map<String, dynamic> json) =>
      _$ReactivationRentFromJson(json);

  const ReactivationRent(
      {required this.price,});

  Map<String, dynamic> toJson() => _$ReactivationRentToJson(this);

  @override
  List<Object?> get props => [price];

// узнать доступна ли аренда
// https://sms-activate.org/stubs/handler_api.php?refresh_token=a12062a62ce1197a4fe629184634386a&sessionId=641357a9e59639d667a759e236b97900&owner=5&userid=1079031&rent_time=4&action=getContinueRentPriceNumber&id=9018730
//{"status":"success","price":13.8}
// {"status":"error","message":"RENT_DIE"}
// реактивировать аренду
// https://sms-activate.org/stubs/handler_api.php?owner=5&rent_time=4&action=continueRentNumber&sessionId=bf59444e5735635cf5f9a6e58d124240&id=8970985&userid=1155497
}
