part of 'reactivation.dart';

Reactivation _$ReactivationFromJson(Map<String, dynamic> json) => Reactivation(
      msg: json['msg'] as String,
      cost: json['cost'].toString(),
      service: json['service'] as String,
      phone: json['phone'] as String,
      country: json['country'] as String,
    );

Map<String, dynamic> _$ReactivationToJson(Reactivation instance) =>
    <String, dynamic>{
      'msg': instance.msg,
      'cost': instance.cost,
      'service': instance.service,
      'phone': instance.phone,
      'country': instance.country,
    };
