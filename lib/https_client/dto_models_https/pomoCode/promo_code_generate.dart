part of 'promo_code.dart';

PromoCode _$PromoCodeFromJson(Map<String, dynamic> json) {
  return PromoCode(
    money: json['money'] == null ? "" : json['money'].toString(),
    rank: json['rank'] == null || json['rank'].toString() == "0"
        ? ""
        : json['rank'].toString(),
    add50k: json['50k'] == null ? false : json['50k'] as bool,
    rankDiscount: json['rank_discount'] == null ? "" : json['rank_discount'].toString(),
  );
}

Map<String, dynamic> _$PromoCodeToJson(PromoCode instance) => <String, dynamic>{
      'money': instance.money,
      'newRank': instance.rank,
      'add50k': instance.add50k,
      'rankDiscount': instance.rankDiscount,
    };
