import 'package:json_annotation/json_annotation.dart';

part 'promo_code_generate.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class PromoCode {
  final String money;
  final String rank;
  @JsonKey(name: "50k")
  final bool add50k;
  @JsonKey(name: "rank_discount")
  final String rankDiscount;

  factory PromoCode.fromJson(final Map<String, dynamic> json) =>
      _$PromoCodeFromJson(json);

  const PromoCode({
    required this.money,
    required this.rank,
    required this.add50k,
    required this.rankDiscount,
  });

  Map<String, dynamic> toJson() => _$PromoCodeToJson(this);

  @override
  List<Object?> get props => [
        money,
        rank,
        add50k,
        rankDiscount,
      ];
}
