part of 'rent_count_and_price_in_time.dart';

RentCountAndPriceInTime _$RentCountAndPriceInTimeFromJson(
    Map<String, dynamic> json) =>
    RentCountAndPriceInTime(
      id: json['id'] as String?,
      count: json['count'] as int,
      price: json['price'] as String,
      retailPrice: json['retail_price'].toString(),
    );

Map<String, dynamic> _$RentCountAndPriceInTimeToJson(
    RentCountAndPriceInTime instance) =>
    <String, dynamic>{
      'id': instance.id,
      'count': instance.count,
      'price': instance.price,
      'retail_price': instance.retailPrice,
    };