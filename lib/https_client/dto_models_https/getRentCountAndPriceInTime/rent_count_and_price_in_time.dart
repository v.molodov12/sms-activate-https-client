
import 'package:json_annotation/json_annotation.dart';

part 'rent_count_and_price_in_time_generate.dart';

class RentCountAndPriceInTime {
  final String? id;
  final int count;
  final String price;
  @JsonKey(name: "retail_price")
  final String retailPrice;

  const RentCountAndPriceInTime(
      {
        required this.id,
        required this.count,
        required this.price,
        required this.retailPrice,});

  factory RentCountAndPriceInTime.fromJson(final Map<String, dynamic> json) => _$RentCountAndPriceInTimeFromJson(json);

  Map<String, dynamic> toJson() => _$RentCountAndPriceInTimeToJson(this);

  @override
  List<Object?> get props => [
    id,
    count,
    price,
    retailPrice,
  ];
}
