import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'active_activation_org.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class ActiveActivationOrg  extends Equatable{
  final String id;
  final String service;
  final String phone;
  final String? status;
  final List<String>? moreSms;
  final int createDate;
  final int finishDate;
  final String forward;
  final String country;
  final String countryCode;
  final String addSms;
  @JsonKey(name: "code")
  final String? sms;
  final String? forwardNumber;
  @JsonKey(name: "lock_cancel_time")
  final int lockCancelTime;
  final String activationType;
  final String? incomingCallNumber;

  const ActiveActivationOrg({
  required this.id,
  required this.service,
  required this.phone,
  this.status,
  required this.moreSms,
  required this.createDate,
  required this.finishDate,
  required this.forward,
  required this.country,
  required this.countryCode,
  required this.addSms,
  this.sms,
  this.forwardNumber,
  required this.lockCancelTime,
  required this.activationType,
  this.incomingCallNumber,
  });

  //TODO(обернуть в try catch выкинуть ошибку парсинга)
  factory ActiveActivationOrg.fromJson(final Map<String, dynamic> json) => _$ActiveActivationOrgFromJson(json);

  Map<String, dynamic> toJson() => _$ActiveActivationOrgToJson(this);

  @override
  List<Object?> get props => [
  id,
  forward,
  phone,
  service,
  country,
  createDate,
  finishDate,
  forwardNumber,
  activationType,
  ];
}
