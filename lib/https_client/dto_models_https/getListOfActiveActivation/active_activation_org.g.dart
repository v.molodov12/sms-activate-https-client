// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_activation_org.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActiveActivationOrg _$ActiveActivationOrgFromJson(Map<String, dynamic> json) =>
    ActiveActivationOrg(
      id: json['id'] as String,
      service: json['service'] as String,
      phone: json['phone'] as String,
      status: json['status'] as String?,
      moreSms:
          (json['moreSms'] as List<dynamic>?)?.map((e) => e as String).toList(),
      createDate: json['createDate'] as int,
      finishDate: json['finishDate'] as int,
      forward: json['forward'] as String,
      country: json['country'] as String,
      countryCode: json['countryCode'] as String,
      addSms: json['addSms'] as String,
      sms: json['code'] as String?,
      forwardNumber: json['forwardNumber'] as String?,
      lockCancelTime: json['lock_cancel_time'] as int,
      activationType: json['activationType'] as String,
      incomingCallNumber: json['incomingCallNumber'] as String?,
    );

Map<String, dynamic> _$ActiveActivationOrgToJson(
        ActiveActivationOrg instance) =>
    <String, dynamic>{
      'id': instance.id,
      'service': instance.service,
      'phone': instance.phone,
      'status': instance.status,
      'moreSms': instance.moreSms,
      'createDate': instance.createDate,
      'finishDate': instance.finishDate,
      'forward': instance.forward,
      'country': instance.country,
      'countryCode': instance.countryCode,
      'addSms': instance.addSms,
      'code': instance.sms,
      'forwardNumber': instance.forwardNumber,
      'lock_cancel_time': instance.lockCancelTime,
      'activationType': instance.activationType,
      'incomingCallNumber': instance.incomingCallNumber,
    };
