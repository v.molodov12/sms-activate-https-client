// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'special_activations_finish_time.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SpecialActivationsFinishTime _$SpecialActivationsFinishTimeFromJson(
        Map<String, dynamic> json) =>
    SpecialActivationsFinishTime(
      shortNameService: json['shortNameService'] as String,
      country: json['country'] as String,
      minRent: json['minRent'] as int,
    );

Map<String, dynamic> _$SpecialActivationsFinishTimeToJson(
        SpecialActivationsFinishTime instance) =>
    <String, dynamic>{
      'shortNameService': instance.shortNameService,
      'country': instance.country,
      'minRent': instance.minRent,
    };
