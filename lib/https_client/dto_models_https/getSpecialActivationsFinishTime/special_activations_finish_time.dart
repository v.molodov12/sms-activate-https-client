import 'package:json_annotation/json_annotation.dart';

part 'special_activations_finish_time.g.dart';

@JsonSerializable(fieldRename: FieldRename.none)
class SpecialActivationsFinishTime {

  final String shortNameService;
  final String country;
  final int minRent;

  const SpecialActivationsFinishTime(
      {required this.shortNameService,
        required this.country,
        required this.minRent});

  factory SpecialActivationsFinishTime.fromJson(final Map<String, dynamic> json) => _$SpecialActivationsFinishTimeFromJson(json);

  Map<String, dynamic> toJson() => _$SpecialActivationsFinishTimeToJson(this);

  @override
  List<Object?> get props => [
    shortNameService,
    country,
    minRent
  ];
}
