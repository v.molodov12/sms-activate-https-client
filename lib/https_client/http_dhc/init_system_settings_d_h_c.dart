import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/data_class/all_services_and_countries.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/advertisingBanners/banners_message.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/appUpdate/app_update.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class InitSystemSettingsDHC {
  final SmsActivateHttpClient http;

  const InitSystemSettingsDHC({required this.http});

  Future<AllServicesAndCountries> getAllServicesAndAllCountries(
      [dynamic arguments]) async {
    final data = await http.getDataDHS(
      builder: (data) {
        return json.decode(data);
      },
      uri: RequestDomainEnums.apiMobile.domain(),
      method: 'get',
      addUserParams: false,
      queryParams: {'action': 'getAllServicesAndAllCountries'},
    );
    if (data != null) {
      AllServicesAndCountries allServicesAndCountries =
          AllServicesAndCountries(data);
      return allServicesAndCountries;
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<BannersMessage> getAdvertisingBanners(String regionCode) async {
    final Map<String, dynamic> data = await http.getDataDHS(
      uri: "https://proxy.sms-activate.org/banner",
      builder: (data) => data,
      method: 'get',
      addUserParams: false,
      queryParams: {
        'code': regionCode.toLowerCase(),
      },
    );
    if (data.isNotEmpty && data["status"] == "success") {
      return BannersMessage.fromJson(data["message"]);
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<String> getCountryRegionCode() async {
    try {
      final Map<String, dynamic> response = await http.getDataDHS(
        uri: "http://ip-api.com/json",
        builder: (data) => data,
        method: 'get',
        addUserParams: false,
      );
      return response["countryCode"];
    } catch (_, __) {
      return "en";
        //LocaleProvider().locale?.languageCode.split("_")[1] ?? Platform.localeName.split("_")[1];
    }
  }

  Future<AppUpdate> getAppUpdate(int marketId) async {
    final Map<String, dynamic> data = await http.getDataDHS(
        uri: RequestDomainEnums.apiMobile.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addUserParams: false,
        queryParams: {
          'key': '856439956d67530bf2846203e876f0dc',
          'market_id': marketId,
          'action': 'getMarketById'
        });

    if (data.isNotEmpty) {
      return AppUpdate.fromJson(data);
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }
}
