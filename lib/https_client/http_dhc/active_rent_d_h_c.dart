import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getActivationRentStatus/activation_rent_status.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getLiveActivationRent/live_activation_rent.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getRentNumber/get_rent_number.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getRentNumber/get_rent_number_tg.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/activations_client_rent_status.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class ActiveRentDHC {
  final SmsActivateHttpClient http;

  const ActiveRentDHC({required this.http});

  Future<List<LiveActivationRent>> getListOfActiveRent(
      [int start = 0, int lightGetList = 50]) async {
    final List<LiveActivationRent> activeActivationsRent = [];
    final data = await http.getDataDHS(
        uri: http.usingTelegramRestApi
            ? RequestDomainEnums.handlerTelegram.domain()
            : RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addTelegramApi:http.usingTelegramRestApi,
        addUserParams:!http.usingTelegramRestApi,
        queryParams: {
          'action': 'getListOfActiveRent',
          'start': start,
          'length': lightGetList
        });

    if (data["data"] != null) {
      data["data"].forEach((liveActivationRent) {
        activeActivationsRent
            .add(LiveActivationRent.fromJson(liveActivationRent));
      });
    }
    return activeActivationsRent;
  }

  Future<GetRentNumber> getNumberRent(
      final int country, final String serviceId, final int hours,
      [String operator = "any"]
      ) async {
    if(http.usingTelegramRestApi){
      return await _getNumberRentTelegramBot(country,serviceId,hours,operator);
    } else{
      return await _getNumberRent(country,serviceId,hours,operator);
    }
  }

  Future<GetRentNumber> _getNumberRent(
      final int country, final String serviceId, final int hours,
      [String operator = "any"]) async {
    Map<String, Object> queryParams = {
      'country': country,
      'service': serviceId,
      'action': 'getRentNumber',
      'operator': operator,
      'rent_time': hours
    };

    var data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerApi.domain(),
      builder: (data) => json.decode(data),
      method: 'get',
      addTelegramApi:http.usingTelegramRestApi,
      addUserParams:!http.usingTelegramRestApi,
      queryParams: queryParams,
    );
    final dataMap = data as Map;
    if (dataMap.values.first == "success") {
      return GetRentNumber.fromJson(dataMap.values.last);
    } else {
      if (dataMap.length < 2) {
        throw ExceptionRepository()
            .getExceptionByName({'error': dataMap.values.first});
      }
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<GetRentNumber> _getNumberRentTelegramBot(
      final int country, final String serviceId, final int hours,
      [String operator = "any"]) async {
    Map<String, Object> queryParams = {
      'country': country,
      'service': serviceId,
      'action': 'getRentNumberWhiteLabel',
      'operator': operator,
      'verification': 0,
      'hours': hours
    };
    //TODO(ПХП нужно поментять ответ с """{"status":"error","code":"1","msg":"\u041d\u0435\u0442 \u043d\u043e\u043c\u0435\u0440\u043e\u0432"}"""; на код ошибки)
    var data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerTelegram.domain(),
      builder: (data) => data,
      method: 'get',
      queryParams: queryParams,
      addTelegramApi:http.usingTelegramRestApi,
      addUserParams:!http.usingTelegramRestApi,
    );
    if (data.toString().contains("id")) {
      Map<String, dynamic> dataMap = json.decode(data);
      return GetRentNumber.adapter(
          GetRentNumberTg.fromJson(dataMap));
    } else {
      if (data.toString().contains("error")) {
        Map<String, dynamic> dataMap = json.decode(data);
        if(dataMap.values.firstOrNull is String){
          throw ExceptionRepository().getExceptionByName(dataMap);
        } else {
          throw ExceptionRepository().getExceptionByName(dataMap.values.firstOrNull);
        }
      } else {
        throw ExceptionRepository()
            .getExceptionByName({'error': data});
      }
    }
  }

  Future<List<ActivationRentStatus>> getStatusRent(
      final String idActivationRent) async {
    final List<ActivationRentStatus> activationRentStatus = [];
    final data = await http.getDataDHS(
        uri: http.usingTelegramRestApi
            ? RequestDomainEnums.handlerTelegram.domain()
            : RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addTelegramApi:http.usingTelegramRestApi,
        addUserParams:!http.usingTelegramRestApi,
        queryParams: {'action': 'getRentStatus', 'id': idActivationRent});

    //TODO(преписать это)
    if (data['message'] != null && data['message'] == 'STATUS_WAIT_CODE') {
      activationRentStatus.add(const ActivationRentStatus(
          phone: "", serviceName: "", textSms: "", date: ""));
    } else {
      if (data["values"] != null) {
        data["values"].forEach((key, liveActivationRent) {
          activationRentStatus
              .add(ActivationRentStatus.fromJson(liveActivationRent));
        });
      }
    }
    return activationRentStatus;
  }

  Future<StatusRent> setStatusRent(
      final String idActivationRent, int statusRent) async {
    final data = await http.getDataDHS(
        uri: http.usingTelegramRestApi
            ? RequestDomainEnums.handlerTelegram.domain()
            : RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addTelegramApi:http.usingTelegramRestApi,
        addUserParams:!http.usingTelegramRestApi,
        queryParams: {
          'action': 'setRentStatus',
          'id': idActivationRent,
          'status': statusRent
        });
    final dataMap = data as Map;
    if (dataMap.values.first == "success") {
      return ActivationsServerRentStatus.success
          .setMessage(dataMap.values.last);
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }
}
