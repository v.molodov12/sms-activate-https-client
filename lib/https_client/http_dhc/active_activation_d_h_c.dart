import 'dart:convert';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/data_class/buy_number_info.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getListOfActiveActivation/active_activation_org.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getNumberActivation/number_activation.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getNumberActivation/number_activation_tg.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/activations_client_rent_status.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class ActiveActivationDHC {
  final SmsActivateHttpClient http;

  const ActiveActivationDHC({required this.http});

  Future<NumberActivation> getNumber(
    BuyNumberInfo buyNumberInfo,
  ) async {
    if (http.usingTelegramRestApi) {
      return await _getNumberTelegramBot(buyNumberInfo);
    } else {
      return await _getNumberActivation(buyNumberInfo);
    }
  }

  Future<NumberActivation> _getNumberActivation(
    BuyNumberInfo buyNumberInfo,
  ) async {
    Map<String, Object> queryParams = {
      'country': buyNumberInfo.countryInfo.id,
      'service': buyNumberInfo.serviceInfo.shortName,
      'action': 'getNumberMobile',
      'forward': buyNumberInfo.serviceInfo.forward,
      'operator': buyNumberInfo.currentOperatorId,
      'activationType': buyNumberInfo.activationType.index,
    };

    if (buyNumberInfo.currentRentalSegment.freePrice) {
      queryParams["maxPrice"] = buyNumberInfo.currentFreePrice;
    }

    var data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerApi.domain(),
      builder: (data) => json.decode(data),
      method: 'get',
      addTelegramApi: http.usingTelegramRestApi,
      addUserParams: !http.usingTelegramRestApi,
      queryParams: queryParams,
    );

    final dataMap = data as Map<String, dynamic>;
    if (dataMap.values.first == "success") {
      return NumberActivation.fromJson(dataMap);
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<NumberActivation> _getNumberTelegramBot(
      BuyNumberInfo buyNumberInfo,
  ) async {
    Map<String, Object> queryParams = {
      'country': buyNumberInfo.countryInfo.id,
      'service': buyNumberInfo.serviceInfo.shortName,
      'forward': buyNumberInfo.serviceInfo.forward,
      'action': 'getNumberWhiteLabel',
      'operator': buyNumberInfo.currentOperatorId,
      'verification': 0,
    };

    if (buyNumberInfo.currentRentalSegment.freePrice) {
      queryParams["maxPrice"] = buyNumberInfo.currentFreePrice;
    }

    var data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerTelegram.domain(),
      builder: (data) => data,
      method: 'get',
      queryParams: queryParams,
      addTelegramApi: http.usingTelegramRestApi,
      addUserParams: !http.usingTelegramRestApi,
    );
    //TODO(Важно поправить это когда будет возможность пример ответа от сервер -- """{"id":"2350830645","userid":"9797869","service":{"ru":"Telegram","en":"Telegram","keywords":",TG,\u0422\u0413,\u0442\u0435\u043b\u0435\u0433\u0440\u0430\u043c,\u0442\u0435\u043b\u0435\u0433\u0440\u0430\u043c\u043c,ntktuhfv,ntktuhfvv,\u0442\u0435\u043b\u0435\u0433\u0430,ntktuf","id":"5","sellTop":"2"},"phone":"79315357546","cost":"165.00","status":"4","code":"","code2":"","moreCodes":"","moreSms":"","createDate":"2024-04-23 15:25:38","receiveSmsDate":"0000-00-00 00:00:00","finishDate":"0000-00-00 00:00:00","forward":"0","ref":"","discount":"0","repeat":"0","simId":"2769554286","port":"-3","country":{"ru":"\u0420\u043e\u0441\u0441\u0438\u044f","en":"Russia","cn":"\u4fc4\u7f57\u65af","ms":5,"phoneCode":"7","regionCode":"RU","addSms":true,"rent":true,"rentNullCount":0},"status_code":"4","key":"tg","sms":"","activationEnd":"2024-04-23 15:45:38","call":""}""";)
    if (data.toString().contains("id")) {
      Map<String, dynamic> dataMap = json.decode(data);
      var result = NumberActivation.adapter(
          NumberActivationTg.fromJson(dataMap));
      return result;
    } else {
      if (data.toString().contains("error")) {
        Map<String, dynamic> dataMap = json.decode(data);
        throw ExceptionRepository().getExceptionByName(dataMap);
      } else {
        throw ExceptionRepository().getExceptionByName({'error': data});
      }
    }
  }

  Future<StatusRent> setStatusActivation(
      final String activationId, final int status,
      [String? forwardNumber]) async {
    late dynamic data;
    if (forwardNumber != null) {
      data = await http.getDataDHS(
          uri: http.usingTelegramRestApi
              ? RequestDomainEnums.handlerTelegram.domain()
              : RequestDomainEnums.handlerApi.domain(),
          builder: (data) => data,
          method: 'get',
          addTelegramApi: http.usingTelegramRestApi,
          addUserParams: !http.usingTelegramRestApi,
          queryParams: {
            'action': 'setStatus',
            'id': activationId,
            'status': status,
            'forward': forwardNumber,
          });
    } else {
      data = await http.getDataDHS(
          uri: http.usingTelegramRestApi
              ? RequestDomainEnums.handlerTelegram.domain()
              : RequestDomainEnums.handlerApi.domain(),
          builder: (data) => data,
          method: 'get',
          addTelegramApi: http.usingTelegramRestApi,
          addUserParams: !http.usingTelegramRestApi,
          queryParams: {
            'action': 'setStatus',
            'id': activationId,
            'status': status
          });
    }

    final result = data as String;
    if (result == "ACCESS_CANCEL" ||
        result == "ACCESS_READY" ||
        result == "ACCESS_RETRY_GET" ||
        result == "ACCESS_ACTIVATION" ||
        result == "ACCESS_CANCEL" ||
        result == "SMS_ADDED") {
      return ActivationsServerRentStatus.success.setMessage(result);
    } else {
      throw ExceptionRepository().getExceptionByName({'error': result});
    }
  }

  Future<List<ActiveActivationOrg>> getListOfActiveActivationsOrg([
    int activationType = 0,
    int start = 0,
    int lightGetList = 50,
  ]) async {
    final List<ActiveActivationOrg> activeActivations = [];
    final data = await http.getDataDHS(
        uri: http.usingTelegramRestApi
            ? RequestDomainEnums.handlerTelegram.domain()
            : RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addTelegramApi: http.usingTelegramRestApi,
        addUserParams: !http.usingTelegramRestApi,
        queryParams: {
          'action': 'getListOfActiveActivations',
          'start': start,
          'length': lightGetList,
          if (!http.usingTelegramRestApi) 'activationType': activationType,
        });

    if (data['data'] != null) {
      data['data'].forEach(
        (liveActivation) {
          activeActivations.add(ActiveActivationOrg.fromJson(liveActivation));
        },
      );
    }

    return activeActivations;
  }

  Future<bool> setTargetForwardNumber(
      final String id, final String number) async {
    Map<String, dynamic> data = await http.getDataDHS(
        uri: http.usingTelegramRestApi
            ? RequestDomainEnums.handlerTelegram.domain()
            : RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addTelegramApi: http.usingTelegramRestApi,
        addUserParams: !http.usingTelegramRestApi,
        queryParams: {
          'action': 'setTargetForwardNumber',
          'activationId': id,
          'number': number
        });

    if (data.isNotEmpty && data["status"] == "success") {
      return true;
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }
}
