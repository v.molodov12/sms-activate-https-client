import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/news_banner/news_banner.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class NewBannerDHC {
  final SmsActivateHttpClient http;
  const NewBannerDHC({required this.http});

  Future<List<NewsBanner>> getAllNewsBanners() async {
    List<NewsBanner> listDTO = [];
    final dataInfo = await json.decode("""[
  {
    "id_banner": 1111,
    "version": 6,
    "image_url": "https://smsactivate.s3.eu-central-1.amazonaws.com/android/Gift-High-Quality-PNG.png",
    "text": {
      "ru": "Получите Activate Premium в подарок при пополнении баланса на 2000 монет и более.",
      "en": "Get Activate Premium as a gift when you top up your balance with 2000 coins or more.",
      "ch": "充值2000枚硬币或以上时，获得Activate Premium礼物。"
    },
    "description": {
      "ru": "",
      "en": "",
      "ch": ""
    },
    "button_text": {
      "ru": "Пополнить",
      "en": "Replenish",
      "ch": "补充你的平衡"
    },
    "action_url": "url",
    "button_action": "",
    "action": {
      "action": "open_page",
      "open_url": "",
      "open_page": "open_page_balance",
      "service_short_name": "tg_0"
    }
  }
]""");
    // final dataInfo = await http.getDataDHS(
    //     uri: RequestDomainEnums.handlerAuth.domain(),
    //     builder: (data) => json.decode(data),
    //     method: 'get',
    //     addUserParams: true,
    //     queryParams: {
    //       'action': 'getAdvertisingBanner',
    //     });

    if (dataInfo.isNotEmpty && dataInfo is List) {
      for (var element in dataInfo) {
        NewsBanner? result = NewsBanner.fromJson(element);
        if(result!=null){
          listDTO.add(
            result,
          );
        }
      }
      return listDTO;
    } else {
      if (dataInfo.isNotEmpty && dataInfo["status"] == "error") {
        throw ExceptionRepository().getExceptionByName(dataInfo);
      } else {
        throw ExceptionRepository().getExceptionByName({
          'error': dataInfo,
        });
      }
    }
  }
  Future<int> getVersionNewsBanner([int? bannerId]) async {
    int idBanner = bannerId??1111;
    String d  = """{"status":"success","1111":6}""";
    final dataInfo =  json.decode(d);
    // final dataInfo = await http.getDataDHS(
    //     uri: RequestDomainEnums.handlerAuth.domain(),
    //     builder: (data) => json.decode(data),
    //     method: 'get',
    //     addUserParams: true,
    //     queryParams: {
    //       'action': 'getVersionAdvertisingBanner',
    //       'id_banner':idBanner,
    //     });

    if (dataInfo.isNotEmpty && dataInfo["status"] == "success") {
      if(dataInfo["$idBanner"] != null){
        return dataInfo["$idBanner"]!;
      }else{
        throw ExceptionRepository().getExceptionByName({
          'error': dataInfo,
        });
      }
    } else {
      if (dataInfo.isNotEmpty && dataInfo["status"] == "error") {
        throw ExceptionRepository().getExceptionByName(dataInfo);
      } else {
        throw ExceptionRepository().getExceptionByName({
          'error': dataInfo,
        });
      }
    }
  }
}
