import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/pars_json_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';
import 'package:sms_activate_https_client/https_client/data_class/user_id_d_c.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class InitUserDHC {
  final SmsActivateHttpClient http;

  const InitUserDHC({required this.http});

  Future<String> getForwardPhone([UserIdDC? user]) async {
    String nameParam = "";
    String param = "";

    if (user == null) {
      throw AccessDenied();
    } else {
      if (user.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = user.telegramId;
      } else {
        nameParam = "email";
        param = user.email;
      }
    }
    String? data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerAuth.domain(),
      queryParams: {
        nameParam: param,
        'action': 'getForwardNumber',
      },
      builder: (data) =>
          int.tryParse(data.replaceAll(RegExp(r"[^0-9]"), "")).toString(),
      method: 'get',
    );
    if (data != null) {
      return data;
    } else {
      throw ExceptionRepository().getExceptionByName({'error': data});
    }
  }

  Future<Map<String, dynamic>> getBalance() async {
    dynamic data ;
    if (http.usingTelegramRestApi) {
      data = await http.getDataDHS(
          uri: RequestDomainEnums.handlerTelegram.domain(),
          builder: (data) => json.decode(data),
          method: 'get',
          addTelegramApi:http.usingTelegramRestApi,
          addUserParams:!http.usingTelegramRestApi,
          queryParams: {'action': "getBalanceAndCashBackV2"},
      );
    } else {
      data = await http.getDataDHS(
          uri: RequestDomainEnums.handlerApi.domain(),
          builder: (data) => json.decode(data),
          method: 'get',
          queryParams: {'action': "getBalanceAndCashBackV2"},
      );
    }

    if (data != null) {
      return data;
    } else {
      throw ExceptionRepository().getExceptionByName({'error': data});
    }
  }

  Future<int> getRank() async {
    Map<String, dynamic> data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerApi.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        queryParams: {'action': 'getRank'});

    if (data['status'] == 'success') {
      try {
        int put = data['mobile_rank'] ? -1 : int.parse(data['rank'].toString());
        return put;
      } catch (_) {
        throw ParsJsonException();
      }
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<bool> updateForwardNumber(final String newNumber,
      [UserIdDC? user]) async {
    String nameParam = "";
    String param = "";

    if (user == null) {
      throw AccessDenied();
    } else {
      if (user.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = user.telegramId;
      } else {
        nameParam = "email";
        param = user.email;
      }
    }

    var _data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => data,
        method: 'get',
        queryParams: {
          'action': 'updateForwardNumber',
          nameParam: param,
          'number': newNumber
        });
    if (_data.isNotEmpty && _data == 'CHANGE_SUCCESS') {
      return true;
    } else {
      throw ExceptionRepository().getExceptionByName({'error': _data});
    }
  }

  Future<int> getUserChannels([UserIdDC? user]) async {
    String nameParam = "";
    String param = "";

    if (user == null) {
      throw AccessDenied();
    } else {
      if (user.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = user.telegramId;
      } else {
        nameParam = "email";
        param = user.email;
      }
    }
    final Map<String, dynamic> data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        queryParams: {
          'action': 'getUserChannels',
          nameParam: param,
        });

    if (data.isNotEmpty && data["status"] == "success") {
      return data["channels"] as int;
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }

  Future<String> getRefBalance([UserIdDC? user]) async {
    String nameParam = "";
    String param = "";

    if (user == null) {
      throw AccessDenied();
    } else {
      if (user.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = user.telegramId;
      } else {
        nameParam = "email";
        param = user.email;
      }
    }
    Map<String, dynamic> data = await http.getDataDHS(
      uri: RequestDomainEnums.handlerAuth.domain(),
      builder: (data) => json.decode(data),
      method: 'get',
      queryParams: {
        'action': 'getReferralBalance',
        nameParam: param,
      },
    );

    if (data.isNotEmpty && data['status'] == 'success') {
      String balance = data['refBalance'].toString();
      return balance;
    } else {
      throw ExceptionRepository().getExceptionByName(data);
    }
  }
}
