import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/pars_json_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';
import 'package:sms_activate_https_client/https_client/data_class/user_id_d_c.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/user/user_profile_model.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class AuthDHC {
  final SmsActivateHttpClient http;

  const AuthDHC({required this.http});

  Future<ServerUser> confirmEmailByCode(
      final String email, final String code, final String twoAuthCode,
      [String deviceId = ""]) async {
    var data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => data,
        method: 'get',
        addUserParams: false,
        queryParams: {
          'email': email,
          'code': code,
          'code_two_auth': twoAuthCode,
          'action': 'confirmEmailByCode',
          'device_id': deviceId
        });

    try {
      final Map<String, dynamic> result = json.decode(data);
      final ServerUser profileInfo = ServerUser.fromJson(result);
      return profileInfo;
    } catch (ex) {
      throw ExceptionRepository().getExceptionByName({"error": data});
    }
  }

  Future<bool> loginOrRegistrationByCode(final String email) async {
    String? data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => data,
        method: 'get',
        addUserParams: false,
        queryParams: {
          'email': email,
          'action': 'loginOrRegistrationByCode',
        });
    if (data != null) {
      switch (data) {
        case "CODE_SUCCESS":
          return false;
        case "REG_MAIL_SUCCESS":
          return false;
        case "2FA_ENABLED":
          return true;
        default:
          throw ExceptionRepository().getExceptionByName({"error": data});
      }
    }
    throw ExceptionRepository().getExceptionByName({"error": data});
  }

  Future<ServerUser> loginOrRegistrationByAppleGoogle({
    required final String deviceId,
    required final String accessToken,
    required final String typeToken,
  }) async {
    String? data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => data,
        method: 'post',
        addUserParams: false,
        postParams: {
          'device_id': deviceId,
          'access_token': accessToken,
          'type_token': typeToken,
          'action': 'nativeSignIn',
        });
    if (data != null) {
      try {
        final Map<String, dynamic> result = json.decode(data);
        final ServerUser profileInfo = ServerUser.fromJson(result);
        return profileInfo;
      } catch (ex) {
        throw ParsJsonException();
      }
    }
    throw ExceptionRepository().getExceptionByName({"error": data});
  }

  Future<bool> deleteUser([
    UserIdDC? user,
  ]) async {
    String nameParam = "";
    String param = "";

    if (user == null) {
      throw AccessDenied();
    } else {
      if (user.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = user.telegramId;
      } else {
        nameParam = "email";
        param = user.email;
      }
    }

    String? data = await http.getDataDHS(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => data,
        method: 'get',
        addUserParams: true,
        queryParams: {
          'action': 'deleteAccount',
          // 'sessionId': validUser.sessionId,
          nameParam: param,
        });
    if (data != null) {
      try {
        Map<dynamic, dynamic> dataMap = json.decode(data);
        if (dataMap["status"] != null) {
          if (dataMap["status"] == "success") {
            return true;
          }
        }
        return false;
      } catch (ex) {
        throw ParsJsonException();
      }
    }
    throw ExceptionRepository().getExceptionByName({"error": data});
  }

  Future<bool> sessionExtension({
    required int botId,
    int ref = 1,
    required telegramId,
  }) async {
    Map<String,dynamic> formData = {
      "ref": ref,
      "botId":botId,
      "telegramId":telegramId
    };

    await http.getDataDHS(
      uri:  RequestDomainEnums.handlerSessionTelegram.domain(),
      builder: (data) => data,
      method: 'post',
      addUserParams: false,
      addTelegramApi: false,
      postParams: formData,
    );

    return true;
  }
}
