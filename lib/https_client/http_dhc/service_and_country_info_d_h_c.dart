import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/countries/server_top_country.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getRentCountAndPriceInTime/rent_count_and_price_in_time.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/getSpecialActivationsFinishTime/special_activations_finish_time.dart';
import 'package:sms_activate_https_client/https_client/dto_models_https/services/server_service_with_price.dart';
import 'package:sms_activate_https_client/https_client/sms_activate_http_client.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class ServiceAndCountryInfoDHC {
  final SmsActivateHttpClient http;

  const ServiceAndCountryInfoDHC({required this.http});

  Future<List<ServerServiceWithPrice>> getTopServiceWithMinPrice() async {
    List<ServerServiceWithPrice> listTopServiceWithMinPrice = [];
    final data = await http.getDataDHS(
        uri: RequestDomainEnums.apiMobile.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addUserParams: false,
        queryParams: {
          'action': 'getAllServices',
          'owner': 5,
        });
    if (data != null) {
      data.forEach((serviceData) {
        listTopServiceWithMinPrice
            .add(ServerServiceWithPrice.fromJson(serviceData));
      });
    }
    return listTopServiceWithMinPrice;
  }

  Future<List<ServerTopCountry>> getTopCountriesByService(
    final String serviceShort,
    final String forward,
  ) async {
    List<ServerTopCountry> serverTopCountryList = [];
    Map<String, dynamic> _data = await http.getDataDHS(
        uri: RequestDomainEnums.apiMobile.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addUserParams: false,
        queryParams: {
          'service': serviceShort,
          'forward': forward,
          'action': 'getTopCountriesByServiceV3',
          'freePrice': 'true',
          'owner': 5,
        });

    if (_data.isNotEmpty) {
      _data.forEach((key, value) {
        int userRank = int.tryParse(http.getSecurityData().rank) ?? 0;
        serverTopCountryList.add(
          ServerTopCountry.fromJson(value, int.parse(key), userRank),
        );
      });
    } else {
      if (_data["status"] == "error") {
        throw ExceptionRepository().getExceptionByName(_data);
      }
    }
    serverTopCountryList.sort((a, b) => a.index.compareTo(b.index));
    return serverTopCountryList;
  }

  Future<List<RentCountAndPriceInTime>> getListRentCountAndPrice(
      String service, String country) async {
    //TODO(изменить ответ от сервера, превести его в правельный вид)
    final List<RentCountAndPriceInTime> listRentCountAndPriceInTime = [];
    final data = await http.getDataDHS(
        uri: RequestDomainEnums.apiMobile.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addUserParams: false,
        queryParams: {
          'action': 'getCountForRentByCountryService',
          'country': country,
          'service': service
        });

    data.forEach((key, countAndPriceInTimeData) {
      if ((key == "warning" && countAndPriceInTimeData == "NO_DATA") ||
          key == "error") {
        // log("$key = $countAndPriceInTimeData -------- fun getListRentCountAndPrice($service,$country)");
        //log помогающий найти сломанный сервис, при получении списка доступных аренд
        return listRentCountAndPriceInTime;
      }

      var element = RentCountAndPriceInTime.fromJson(countAndPriceInTimeData);
      listRentCountAndPriceInTime.add(RentCountAndPriceInTime(
          id: key,
          price: element.price,
          count: element.count,
          retailPrice: element.retailPrice));
    });

    return listRentCountAndPriceInTime;
  }

  Future<Map<String, Map<String, SpecialActivationsFinishTime>>>
      getSpecialActivationsFinishTime() async {
    final Map<String, Map<String, SpecialActivationsFinishTime>>
        mapSpecialFinishTime = {};
    final data = await http.getDataDHS(
        uri: RequestDomainEnums.apiMobile.domain(),
        builder: (data) => json.decode(data),
        method: 'get',
        addUserParams: false,
        queryParams: {
          'action': 'getSpecialActivationsFinishTime',
        });

    data.forEach((key, value) {
      Map v = value;
      if (v.keys.length > 1) {
        for (var element in v.keys) {
          SpecialActivationsFinishTime finishTimeElement =
              SpecialActivationsFinishTime(
            shortNameService: key,
            country: element,
            minRent: int.parse(v[element]),
          );
          // Добавление элемента в карту mapSpecialFinishTime
          mapSpecialFinishTime[element] ??= {};
          mapSpecialFinishTime[element]![key] = finishTimeElement;
        }
      } else {
        SpecialActivationsFinishTime finishTimeElement =
        SpecialActivationsFinishTime(
          shortNameService: key,
          country: v.keys.first,
          minRent: int.parse(v[v.keys.first]),
        );
        // Добавление элемента в карту mapSpecialFinishTime
        mapSpecialFinishTime[v.keys.first] ??= {};
        mapSpecialFinishTime[v.keys.first]![key] = finishTimeElement;
      }
    });

    return mapSpecialFinishTime;
  }
}
