import 'dart:convert';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/internet_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/repeat_request_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/update_cookie_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';
import 'package:sms_activate_https_client/https_client/data_class/security_data.dart';
import 'package:sms_activate_https_client/resources/base_enum/request_domain_enums.dart';

class SecurityService {
  SecurityService() {
    _init();
  }

  final Dio _securityHttpsClient = Dio();
  ValueChanged<bool>? _failedUpdateSecurityData;
  ValueChanged<SecurityData>? _successUpdateSecurityData;

  SecurityData _securityData = const SecurityData();
  String _deviceCountryCode = "";

  int _deviceOwner = 5;

  _init() async {
    _deviceOwner = _getOwner();
    _deviceCountryCode = await _getCountryRegionCode();
  }

  setSecurityData({
    required SecurityData newData,
  }) {

    _securityData = _securityData.copyWith(
      refreshToken: newData.refreshToken,
      privateSession: newData.privateSession,
      bot:newData.bot,
      sessionId: newData.sessionId,
      userid: newData.userid,
      telegramId: newData.telegramId,
      email: newData.email,
      rank: newData.rank,
      whiteLabel: newData.whiteLabel,
      telegramApi: newData.telegramApi,
    );
  }

  SecurityData getSecurityData() => _securityData;

  setCallbackUpdate({
    required ValueChanged<bool> failedUpdate,
    required ValueChanged<SecurityData> successUpdate,
  }) {
    _failedUpdateSecurityData = failedUpdate;
    _successUpdateSecurityData = successUpdate;
  }

  Map<String, dynamic> getQueryRequestParams(
    final Map<String, dynamic> queryParams,
    bool addUserOptions,
    bool telegramApi,
  ) {
    final Map<String, dynamic> query = {};

    query.addAll(queryParams);

    if (addUserOptions) {
      if (_securityData.refreshToken.isNotEmpty &&
          _securityData.sessionId.isNotEmpty &&
          _securityData.userid.isNotEmpty) {
        query.addAll({
          'refresh_token': _securityData.refreshToken,
          'sessionId': _securityData.sessionId,
          'userid': _securityData.userid,
        });
      }
      query.addAll({
        'owner': _deviceOwner,
        'lang': _deviceCountryCode,
      });
    }
    if (telegramApi) {
      if (_securityData.privateSession.isNotEmpty &&
          _securityData.sessionId.isNotEmpty &&
          _securityData.userid.isNotEmpty) {
        query.addAll({
          'private_session': Uri.decodeFull(_securityData.privateSession),
          'session': Uri.decodeFull(_securityData.sessionId),
          'user_id': _securityData.userid,
          'bot': _securityData.bot,
          'white_label': _securityData.whiteLabel,
          'telegram': _securityData.telegramId,
        });
      }
    }
    return query;
  }

  int _getOwner() {
    return 8;
    //TODO( add telegram_bot = owner 8)
  }

  Future<String> _getCountryRegionCode() async {
    try {
      final Map<String, dynamic> response = await getData(
        uri: "https://get.geojs.io/v1/ip/geo.json",
        builder: (data) => data,
        addUserParams: false,
      );
      return response["country_code"].toString().toLowerCase();
    } catch (_, __) {
      return "en";
    }
  }


  Future<dynamic> repeatFailedRequest(Uri uri) async {
    try {
      uri.queryParameters["refresh_token"] = _securityData.refreshToken;
      uri.queryParameters["sessionId"] = _securityData.sessionId;

      var data = await getData(
        uri: uri.origin + uri.path,
        builder: (data) => data,
        queryParams: uri.queryParameters,
      );
      return data;
    } on DioError catch (dioError) {
      if (dioError.error is BadSessionException ||
          dioError.error is AccessDenied ||
          dioError.error is SessionException) {
        rethrow;
      } else {
        throw RepeatRequestException();
      }
    } catch (ex) {
      throw RepeatRequestException();
    }
  }

  Future<SecurityData> updateTokenAndSession() async {
    try {
      String nameParam = "";
      String param = "";

      if (_securityData.telegramId.isNotEmpty) {
        nameParam = "telegram_id";
        param = _securityData.telegramId;
      } else {
        nameParam = "email";
        param = _securityData.email;
      }

      final newSession = await getData(
        uri: RequestDomainEnums.handlerAuth.domain(),
        builder: (data) => json.decode(data),
        queryParams: {
          nameParam: param,
          'action': 'updateUserCookie',
        },
      );
      _securityData = _securityData.copyWith(
        refreshToken: newSession['refresh_token'],
        sessionId: newSession['sessionId'],
      );
      if (_successUpdateSecurityData != null) {
        _successUpdateSecurityData!(_securityData);
      }
      return _securityData;
    } catch (ex) {
      if (ex is UpdateCookieException) {
        if (_failedUpdateSecurityData != null) {
          _failedUpdateSecurityData!(true);
        }
      }
      rethrow;
    }
  }

  Future<T> getData<T>({
    required String uri,
    Map<String, Object>? queryParams,
    required T Function(dynamic data) builder,
    bool addUserParams = true,
  }) async {
    try {
      Response response = await _securityHttpsClient.get(
        uri,
        queryParameters: queryParams ?? {},
      );
      switch (response.statusCode) {
        case 200:
          return builder(response.data);
        default:
          throw ServerException();
      }
    } on SocketException catch (_) {
      throw InternetException();
    } on DioError catch (dioError) {
      if (dioError.error is BadSessionException ||
          dioError.error is AccessDenied ||
          dioError.error is SessionException) {
        if (queryParams!["action"] == "updateUserCookie") {
          throw UpdateCookieException();
        }
      }
      rethrow;
    } catch (ex) {
      rethrow;
    }
  }
}
