import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/repeat_request_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/update_cookie_exception.dart';
import 'package:sms_activate_https_client/https_client/service/security_service.dart';
import 'package:validators/validators.dart';

class DefaultErrorInterceptor extends QueuedInterceptor {
  final SecurityService securityService;

  DefaultErrorInterceptor(
      {required this.securityService}
  );

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    /// добавляет куки в запрос
    options.queryParameters = securityService.getQueryRequestParams(
      options.queryParameters,
      options.headers['addUserOptions'] ?? false,
      options.headers['addTelegramApi'] ?? false,
    );

    super.onRequest(options, handler);
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    Response newResponse = response;
    if (response.data is String) {
      if (isJSON(response.data)) {
        final info = json.decode(response.data);
        if (info is Map) {
          if (info['status'] == 'error') {
            switch (info['error']) {
              case "ACCESS_DENIED":
                newResponse = await updateCookie(response);
                break;
              case "SESSION_EXPIRED":
                newResponse = await updateCookie(response);
                break;
              default:
                break;
            }
          }
        }
      }
      if (response.data == "BAD_SESSION") {
        newResponse = await updateCookie(response);
      }
    }
    return super.onResponse(newResponse, handler);
  }

  Future<Response> updateCookie(Response oldResponse) async {
    Response response = oldResponse;
    dynamic data = '';
    try {
      /// обновляет сессию и токин
      await securityService.updateTokenAndSession();
      /// повторяет запрос который вернул ошибку обновление кук
      var result = await securityService.repeatFailedRequest(
        response.realUri,
      );
      /// добовлет новый резудьтат повторного запроса в ответ
      data = result;

    } on UpdateCookieException catch (ex) {
      /// добовляем в ответ результат исключения
      data = ex.toMap().toString();
    } catch (ex) {
      /// если возникла ошибка при повторном запросе,
      /// добовляем в ответ результат исключения
      data = RepeatRequestException().toMap().toString();
    }
    /// Изменяем ответ от сервера
    response = Response(
      data: data,
      headers: oldResponse.headers,
      requestOptions: oldResponse.requestOptions,
      isRedirect: oldResponse.isRedirect,
      statusCode: oldResponse.statusCode,
      statusMessage: oldResponse.statusMessage,
      redirects: oldResponse.redirects,
      extra: oldResponse.extra,
    );
    /// возвращаем изменненый ответ от сервера, чтобы разрешить другие запросы
    return response;
  }
}
