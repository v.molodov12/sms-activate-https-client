import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_repository.dart';
import 'package:validators/validators.dart';

class AlternativeErrorInterceptor extends Interceptor {
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    if (response.data is String) {
      if (isJSON(response.data)) {
        final info = json.decode(response.data);
        if (info is Map) {
          if (info['status'] == 'error') {
            throw ExceptionRepository().getExceptionByName(info);
          }
        }
      }
      if (response.data == "BAD_SESSION") {
        throw ExceptionRepository().getExceptionByName({
          'error': 'BAD_SESSION',
        });
      }
    }
    return super.onResponse(response, handler);
  }
}
