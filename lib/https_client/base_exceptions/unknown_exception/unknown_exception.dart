import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';

class UnknownException extends BaseException {
  @override
  final String name;
  @override
  final Map<String, String> message;

  UnknownException({
    this.name = "UNKNOWN",
    required this.message,
  });

  String getName() {
    return name;
  }

  Map<String, String> getMessage() {
    return message;
  }

  static UnknownException copyWith(Map<String, String> newMessage) {
    if (newMessage.isEmpty) {
      return UnknownException(
        name: "UNKNOWN",
        message: {
          "ru": "Неизвестная ошибка",
          "en": "Unknown error",
          "zh": "未知错误",
        },
      );
    }
    return UnknownException(
      name: "UNKNOWN",
      message: newMessage,
    );
  }

  static UnknownException copyWithTwo(
    String name,
    Map<String, String> newMessage,
  ) {
    return UnknownException(
      name: name.isEmpty ? "UNKNOWN" : name,
      message: newMessage.isEmpty
          ? {
              "ru": "Неизвестная ошибка",
              "en": "Unknown error",
              "zh": "未知错误",
            }
          : newMessage,
    );
  }
}
