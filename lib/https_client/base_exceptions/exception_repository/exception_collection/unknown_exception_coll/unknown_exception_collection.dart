import 'package:flutter/widgets.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/unknown_exception/unknown_exception.dart';

class UnknownExceptionCollection extends BaseExceptionCollection {
  const UnknownExceptionCollection();

  static UnknownException _baseEx = UnknownException(
    name: "UNKNOWN",
    message: {
      "ru": "Неизвестная ошибка",
      "en": "Unknown error",
      "zh": "未知错误",
    },
  );

  static final Map<String, UnknownException> _mapEx = {
    UnknownException(
      name: "UNKNOWN",
      message: {
        "ru": "Неизвестная ошибка",
        "en": "Unknown error",
        "zh": "未知错误",
      },
    ).name: UnknownException(
      name: "UNKNOWN",
      message: {
        "ru": "Неизвестная ошибка",
        "en": "Unknown error",
        "zh": "未知错误",
      },
    ),
  };

  void init(Exception ex) {
    if (ex is UnknownException) {
      _baseEx = ex;
    } else {
      _baseEx = UnknownException.copyWith({
        "ru": ex.toString(),
        "en": ex.toString(),
        "zh": ex.toString(),
      });
    }
  }

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, UnknownException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang] ?? _baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void run(BuildContext context, [bool baseHandler = true]) {
    showSnackBar(context);
  }

  @override
  String getName() {
    return _baseEx.name;
  }
}
