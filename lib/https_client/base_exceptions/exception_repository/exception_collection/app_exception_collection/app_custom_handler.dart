import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/internet_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/pars_json_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/repeat_request_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/update_cookie_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';

class AppCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? internetException;
  final ValueChanged<bool>? parsJsonException;
  final ValueChanged<bool>? repeatRequestException;
  final ValueChanged<bool>? updateCookieException;

  AppCustomHandler({
    this.internetException,
    this.parsJsonException,
    this.repeatRequestException,
    this.updateCookieException,
  }) {
    _mapExType.addAll({
      InternetException: internetException,
      ParsJsonException: parsJsonException,
      RepeatRequestException: repeatRequestException,
      UpdateCookieException: updateCookieException,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(AppException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}