import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/ddos_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/internet_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/pars_json_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/repeat_request_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception_set/update_cookie_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/app_exception_collection/app_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';

class AppExceptionCollection extends BaseExceptionCollection {
  const AppExceptionCollection();

  static AppException _baseEx = AppException();

  static AppCustomHandler _handler = AppCustomHandler();

  static final Map<String, AppException> _mapEx = {
    DDoSException().name: DDoSException(),
    InternetException().name: InternetException(),
    ParsJsonException().name: ParsJsonException(),
    RepeatRequestException().name: RepeatRequestException(),
    UpdateCookieException().name: UpdateCookieException(),
  };

  void init(AppException ex) {
    if (ex.message.isNotEmpty) {
      _baseEx = ex;
    } else {
      _baseEx = AppException();
    }
  }

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, BaseException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang]??_baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void setCustomHandler(AppCustomHandler customHandler) {
    _handler = customHandler;
  }

  void run(BuildContext context,[bool baseHandler = true]) {
    ValueChanged<bool>? result =
    _handler.getHandlerForError(_baseEx);
    if (result != null) {
      result(true);
    } else {
      if (baseHandler) {
        showSnackBar(context);
      }
    }
  }

  @override
  String getName() {
    return _baseEx.name;
  }
}