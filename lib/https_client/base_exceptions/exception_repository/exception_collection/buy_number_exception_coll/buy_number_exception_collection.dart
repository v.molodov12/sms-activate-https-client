import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/buy_number_exception_coll/buy_number_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/buy_number_exception_set/buy_number_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
class BuyNumberExceptionCollection extends BaseExceptionCollection {
  const BuyNumberExceptionCollection();

  static ServerException _baseEx = BuyNumberException();

  static final Map<String, ServerException> _mapEx = {
    BadAction().name: BadAction(),
    NoNumbers().name: NoNumbers(),
    NoBalance().name: NoBalance(),
    BadService().name: BadService(),
    BadKey().name: BadKey(),
    ErrorSql().name: ErrorSql(),
    ErrorWhatsApp().name: ErrorWhatsApp(),
    ErrorYoulaMail().name: ErrorYoulaMail(),
    ErrorByServiceBan().name: ErrorByServiceBan(),
    BadPassword().name: BadPassword(),
    WrongMaxPrice().name: WrongMaxPrice(),
  };

  void init(ServerException ex) {
    if (ex.message.isNotEmpty) {
      _baseEx = ex;
    } else {
      _baseEx = BuyNumberException();
    }
  }

  static ByNumberCustomHandler _handler = ByNumberCustomHandler();

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, BaseException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang]??_baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void setCustomHandler(ByNumberCustomHandler customHandler) {
    _handler = customHandler;
  }

  void run(BuildContext context,[bool baseHandler = true]) {
    ValueChanged<bool>? result =
    _handler.getHandlerForError(_baseEx);
    if (result != null) {
      result(true);
    } else {
      if (baseHandler) {
        showSnackBar(context);
      }
    }
  }
  @override
  String getName() {
    return _baseEx.name;
  }
}