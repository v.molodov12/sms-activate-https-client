import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/buy_number_exception_set/buy_number_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class ByNumberCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? badAction;
  final ValueChanged<bool>? badService;
  final ValueChanged<bool>? badKey;
  final ValueChanged<bool>? errorSql;
  final ValueChanged<bool>? noBalance;
  final ValueChanged<bool>? noNumbers;
  final ValueChanged<bool>? errorWhatsApp;
  final ValueChanged<bool>? errorYoulaMail;
  final ValueChanged<bool>? errorByServiceBan;
  final ValueChanged<bool>? badPassword;
  final ValueChanged<bool>? wrongMaxPrice;

  ByNumberCustomHandler({
    this.badAction,
    this.badService,
    this.badKey,
    this.errorSql,
    this.noBalance,
    this.noNumbers,
    this.errorWhatsApp,
    this.errorYoulaMail,
    this.errorByServiceBan,
    this.badPassword,
    this.wrongMaxPrice,
  }) {
    _mapExType.addAll({
      BadAction: badAction,
      NoNumbers: noNumbers,
      NoBalance: noBalance,
      BadService: badService,
      BadKey: badKey,
      ErrorSql: errorSql,
      ErrorWhatsApp: errorWhatsApp,
      ErrorYoulaMail: errorYoulaMail,
      ErrorByServiceBan: errorByServiceBan,
      BadPassword: badPassword,
      WrongMaxPrice: wrongMaxPrice,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}