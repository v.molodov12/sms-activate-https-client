import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/set_ref_code_set/set_ref_code_exception.dart';

class SetRefCodeCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? noReferrerException;
  final ValueChanged<bool>? accountIsReferredException;
  final ValueChanged<bool>? accountIsOldException;

  SetRefCodeCustomHandler({
    this.noReferrerException,
    this.accountIsReferredException,
    this.accountIsOldException,
  }){
    _mapExType.addAll({
      NoReferrerException: noReferrerException,
      AccountIsReferredException: accountIsReferredException,
      AccountIsOldException: accountIsOldException,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}