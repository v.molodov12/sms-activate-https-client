import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/set_ref_code_coll/set_ref_code_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/set_ref_code_set/set_ref_code_exception.dart';

class SetRefCodeExceptionCollection extends BaseExceptionCollection {
  const SetRefCodeExceptionCollection();

  static ServerException _baseEx = SetRefCodeException();

  static SetRefCodeCustomHandler _handler = SetRefCodeCustomHandler();

  static final Map<String, ServerException> _mapEx = {
    NoReferrerException().name: NoReferrerException(),
    AccountIsReferredException().name: AccountIsReferredException(),
    AccountIsOldException().name: AccountIsOldException(),
  };

  void init(ServerException ex) {
    if (ex.message.isNotEmpty) {
      _baseEx = ex;
    } else {
      _baseEx = NoReferrerException();
    }
  }

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, BaseException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang]??_baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void setCustomHandler(SetRefCodeCustomHandler customHandler) {
    _handler = customHandler;
  }

  void run(BuildContext context,[bool baseHandler = true]) {
    ValueChanged<bool>? result =
    _handler.getHandlerForError(_baseEx);
    if (result != null) {
      result(true);
    } else {
      if (baseHandler) {
        showSnackBar(context);
      }
    }
  }
  @override
  String getName() {
    return _baseEx.name;
  }
}