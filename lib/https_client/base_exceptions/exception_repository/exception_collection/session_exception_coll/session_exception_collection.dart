import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/session_exception_coll/session_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';

class SessionExceptionCollection extends BaseExceptionCollection {
  const SessionExceptionCollection();

  static ServerException _baseEx = SessionAuthException();

  static SessionCustomHandler _handler = SessionCustomHandler();

  static final Map<String, ServerException> _mapEx = {
    SessionException().name: SessionException(),
    BadSessionException().name: BadSessionException(),
    AccessDenied().name: AccessDenied(),
  };

  void init(ServerException ex) {
    if (ex.message.isNotEmpty) {
      _baseEx = ex;
    } else {
      _baseEx = SessionAuthException();
    }
  }

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, BaseException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang]??_baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void setCustomHandler(SessionCustomHandler customHandler) {
    _handler = customHandler;
  }

  void run(BuildContext context,[bool baseHandler = true]) {
    ValueChanged<bool>? result =
    _handler.getHandlerForError(_baseEx);
    if (result != null) {
      result(true);
    } else {
      if (baseHandler) {
        showSnackBar(context);
      }
    }
  }
  @override
  String getName() {
    return _baseEx.name;
  }
}