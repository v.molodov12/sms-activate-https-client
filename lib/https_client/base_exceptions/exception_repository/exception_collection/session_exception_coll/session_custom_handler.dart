import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';

class SessionCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? sessionException;
  final ValueChanged<bool>? badSessionException;
  final ValueChanged<bool>? accessDenied;

  SessionCustomHandler({
    this.sessionException,
    this.badSessionException,
    this.accessDenied,
  }){
    _mapExType.addAll({
      SessionException: sessionException,
      BadSessionException: badSessionException,
      AccessDenied: accessDenied,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}