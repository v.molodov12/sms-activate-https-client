import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/auth_exception_set/auth_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class AuthCustomHandler extends BaseCustomHandler {
  final ValueChanged<bool>? regMailException;
  final ValueChanged<bool>? tooManyAttemptsReg;
  final ValueChanged<bool>? regError;
  final ValueChanged<bool>? badParam;
  final ValueChanged<bool>? tooManyAttemptsMinute;
  final ValueChanged<bool>? tooManyAttemptsHour;
  final ValueChanged<bool>? badEmail;
  final ValueChanged<bool>? codeError;
  final ValueChanged<bool>? noUser;
  final ValueChanged<bool>? badCode;
  final ValueChanged<bool>? codeExpire;
  final ValueChanged<bool>? twoFaError;
  final ValueChanged<bool>? failedToUpdate;

  AuthCustomHandler({
    this.regMailException,
    this.tooManyAttemptsReg,
    this.regError,
    this.badParam,
    this.tooManyAttemptsMinute,
    this.tooManyAttemptsHour,
    this.badEmail,
    this.noUser,
    this.badCode,
    this.codeError,
    this.codeExpire,
    this.twoFaError,
    this.failedToUpdate,
  }) {
    _mapExType.addAll({
      RegMailException: regMailException,
      TooManyAttemptsReg: tooManyAttemptsReg,
      RegError: regError,
      BadParam: badParam,
      TooManyAttemptsMinute: tooManyAttemptsMinute,
      TooManyAttemptsHour: tooManyAttemptsHour,
      BadEmail: badEmail,
      CodeError: noUser,
      NoUser: badCode,
      BadCode: codeError,
      CodeExpire: codeExpire,
      TwoFaError: twoFaError,
      FailedToUpdate: failedToUpdate,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}
