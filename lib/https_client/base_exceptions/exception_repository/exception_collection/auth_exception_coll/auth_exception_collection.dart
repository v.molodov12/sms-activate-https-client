import 'package:flutter/material.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/auth_exception_coll/auth_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/auth_exception_set/auth_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class AuthExceptionCollection extends BaseExceptionCollection {
  const AuthExceptionCollection();

  static ServerException _baseEx = AuthException();

  static final Map<String, ServerException> _mapEx = {
    RegMailException().name: RegMailException(),
    TooManyAttemptsReg().name: TooManyAttemptsReg(),
    RegError().name: RegError(),
    BadParam().name: BadParam(),
    TooManyAttemptsMinute().name: TooManyAttemptsMinute(),
    TooManyAttemptsHour().name: TooManyAttemptsHour(),
    BadEmail().name: BadEmail(),
    CodeError().name: CodeError(),
    NoUser().name: NoUser(),
    BadCode().name: BadCode(),
    CodeExpire().name: CodeExpire(),
    TwoFaError().name: TwoFaError(),
    FailedToUpdate().name: FailedToUpdate(),
    IsDelete().name: IsDelete(),
  };


  void init(ServerException ex) {
    if (ex.message.isNotEmpty) {
      _baseEx = ex;
    } else {
      _baseEx = AuthException();
    }
  }

  static AuthCustomHandler _handler = AuthCustomHandler();

  @override
  Widget getErrorWidget() {
    return const SizedBox();
  }

  @override
  Map<String, BaseException> getMapException() {
    return _mapEx;
  }

  @override
  String getMassage(String lang) {
    return _baseEx.message[lang]??_baseEx.message.values.first;
  }

  @override
  void showSnackBar(BuildContext context) {
    // ThemeData currentTheme = ThemeModelInheritedNotifier.of(context).theme;
    // SnackBarManager(context: context).snackBarAllShow(
    //   TypeSnackBars.info,
    //   Text(
    //     _baseEx.message,
    //     style: currentTheme.textTheme.bodyMedium,
    //   ),
    //   500,
    //   1500,
    // );
  }

  void setCustomHandler(AuthCustomHandler customHandler) {
    _handler = customHandler;
  }

  void run(BuildContext context,[bool baseHandler = true]) {
    ValueChanged<bool>? result =
    _handler.getHandlerForError(_baseEx);
    if (result != null) {
      result(true);
    } else {
      if (baseHandler) {
        showSnackBar(context);
      }
    }
  }

  @override
  String getName() {
    return _baseEx.name;
  }
}