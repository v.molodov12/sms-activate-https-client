import 'package:flutter/widgets.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';

abstract class BaseExceptionCollection {
  const BaseExceptionCollection();

  String getMassage(String lang);

  String getName();

  void showSnackBar(BuildContext context);

  Widget getErrorWidget();

  Map<String,BaseException> getMapException();
}
