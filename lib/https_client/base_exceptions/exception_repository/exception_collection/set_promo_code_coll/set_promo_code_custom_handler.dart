import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/set_promo_code/set_promo_code_exception.dart';

class SetPromoCodeCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? setPromoCodeException;
  final ValueChanged<bool>? wrongCodeException;
  final ValueChanged<bool>? noPromoException;
  final ValueChanged<bool>? maxAttemptsExceedException;
  final ValueChanged<bool>? usedException;

  SetPromoCodeCustomHandler({
    this.setPromoCodeException,
    this.wrongCodeException,
    this.noPromoException,
    this.maxAttemptsExceedException,
    this.usedException,
  }){
    _mapExType.addAll({
      SetPromoCodeException: setPromoCodeException,
      WrongCodeException: wrongCodeException,
      NoPromoException: noPromoException,
      MaxAttemptsExceedException: maxAttemptsExceedException,
      UsedException: usedException,
      DisabledException: maxAttemptsExceedException,
      ExpiredException: maxAttemptsExceedException,
      NonStartedException: maxAttemptsExceedException,
      NoSpendingException: maxAttemptsExceedException,
      IDdNotInListException: maxAttemptsExceedException,
      NoPromoCodeException: maxAttemptsExceedException,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}