import 'package:flutter/cupertino.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_custom_handler.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/reactivation_exception_set/reactivation_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class ReactivationCustomHandler extends BaseCustomHandler{
  final ValueChanged<bool>? reactivationException;
  final ValueChanged<bool>? noAccessToTheNumberException;

  ReactivationCustomHandler({
    this.reactivationException,
    this.noAccessToTheNumberException,
  }){
    _mapExType.addAll({
      ReactivationException: reactivationException,
      NoAccessToTheNumberException: noAccessToTheNumberException,
    });
  }

  static final Map<Type, ValueChanged<bool>?> _mapExType = {};

  ValueChanged<bool>? getHandlerForError(ServerException ex) {
    ValueChanged<bool>? result = _mapExType[ex.runtimeType];
    if (result != null) {
      return result;
    }
    return null;
  }
}