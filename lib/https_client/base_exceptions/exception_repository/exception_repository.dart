import 'dart:convert';

import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/app_exception_collection/app_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/auth_exception_coll/auth_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/base_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/buy_number_exception_coll/buy_number_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/reactivation_exception_coll/reactivation_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/session_exception_coll/session_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/set_promo_code_coll/set_promo_code_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/set_ref_code_coll/set_ref_code_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/exception_repository/exception_collection/unknown_exception_coll/unknown_exception_collection.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/auth_exception_set/auth_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/buy_number_exception_set/buy_number_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/reactivation_exception_set/reactivation_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/session_exception_set/session_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/set_promo_code/set_promo_code_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/set_ref_code_set/set_ref_code_exception.dart';
import 'package:sms_activate_https_client/https_client/base_exceptions/unknown_exception/unknown_exception.dart';

class ExceptionRepository {
  final Map<String, BaseException> _allMapEx = {};
  final byNumberExceptionCollection = const BuyNumberExceptionCollection();
  final authExceptionCollection = const AuthExceptionCollection();
  final reactivationExceptionCollection = const ReactivationExceptionCollection();
  final sessionExceptionCollection = const SessionExceptionCollection();
  final appExceptionCollection = const AppExceptionCollection();
  final unknownExceptionCollection = const UnknownExceptionCollection();
  final setRefCodeExceptionCollection = const SetRefCodeExceptionCollection();
  final setPromoCodeExceptionCollection = const SetPromoCodeExceptionCollection();

  static final ExceptionRepository _singleton = ExceptionRepository._internal();

  ExceptionRepository._internal() {
    if (_allMapEx.isEmpty) {
      _allMapEx.addAll(byNumberExceptionCollection.getMapException());
      _allMapEx.addAll(authExceptionCollection.getMapException());
      _allMapEx.addAll(reactivationExceptionCollection.getMapException());
      _allMapEx.addAll(sessionExceptionCollection.getMapException());
      _allMapEx.addAll(appExceptionCollection.getMapException());
      _allMapEx.addAll(setRefCodeExceptionCollection.getMapException());
      _allMapEx.addAll(setPromoCodeExceptionCollection.getMapException());
    }
  }

  factory ExceptionRepository() {
    return _singleton;
  }

  BaseException getExceptionByName(Map<dynamic, dynamic> nameEx) {
    String errorName = '';
    if(nameEx.isNotEmpty) {
      if(nameEx["message"] != null){
        errorName = nameEx['message'];
      }

      if(nameEx["msg"] != null){
        errorName = nameEx['msg'];
      }

      if(nameEx["error"] != null){
        try{
          Map result = json.decode(errorName);
          errorName = result['error'];
        } catch(_) {
          errorName = nameEx['error'];
        }
      }
    } else {
      errorName = nameEx['error'];
      if (errorName.contains("error")) {
        try{
          Map result = json.decode(errorName);
          errorName = result['error'];
        } catch(_) {
          errorName = nameEx['error'];
        }
      }
    }
    if (errorName.isNotEmpty) {
      BaseException? result = _allMapEx[errorName];
      if (result != null) {
        return result;
      }
    }

    var result =  UnknownException.copyWithTwo(
      errorName,
      nameEx['message'] ?? {"message":nameEx.toString()},
    );
    return result;
  }

  BaseExceptionCollection getCollectionByException(Exception ex) {
    if (ex is ServerException) {
      if (ex is BuyNumberException) {
        return byNumberExceptionCollection..init(ex);
      } else if (ex is AuthException) {
        return authExceptionCollection..init(ex);
      } else if (ex is ReactivationNumberException) {
        return reactivationExceptionCollection..init(ex);
      } else if (ex is SessionAuthException) {
        return sessionExceptionCollection..init(ex);
      }else if (ex is SetRefCodeException) {
        return setRefCodeExceptionCollection..init(ex);
      }else if (ex is SetPromoCodeException) {
        return setPromoCodeExceptionCollection..init(ex);
      }
    } else if (ex is AppException) {
      return appExceptionCollection..init(ex);
    }
    return unknownExceptionCollection..init(ex);
  }
}
