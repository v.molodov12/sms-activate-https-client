import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class AuthException extends ServerException {
  AuthException();
}

class RegMailException extends AuthException {
  @override
  String get name => "REG_MAIL_ERROR";

  @override
  Map<String, String> get message => {
    "ru": "Письмо с подтверждением не удалось отправить",
    "en": "Failed to send confirmation email",
    "zh": "发送确认邮件失败",
  };

  RegMailException();
}

class TooManyAttemptsReg extends AuthException {
  @override
  String get name => "TO_MANY_ATTEMPTS_REG";

  @override
  Map<String, String> get message => {
    "ru": "Превышен лимит запросов на регистрацию данной почты",
    "en": "The limit of registration requests for this mail has been exceeded",
    "zh": "已超出此邮件的注册请求限制",
  };
  TooManyAttemptsReg();
}
class RegError extends AuthException{
  @override
  String get name => "REG_ERROR";

  @override
  Map<String, String> get message => {
    "ru": "Техническая ошибка при регистрации",
    "en": "Technical error during registration",
    "zh": "注册过程中的技术错误",
  };
  RegError();
}
class BadParam extends AuthException{
  @override
  String get name => "BAD_PARAM";

  @override
  Map<String, String> get message => {
    "ru": "Превышено число запросов на регистрацию",
    "en": "Exceeded number of registration requests",
    "zh": "超过注册请求数",
  };
  BadParam();
}
class TooManyAttemptsMinute extends AuthException{
  @override
  String get name => "TO_MANY_ATTEMPTS_MINUTE";

  @override
  Map<String, String> get message => {
    "ru": "Превышено число запросов в минуту на подтверждение входа",
    "en": "Exceeded number of requests per minute for login confirmation",
    "zh": "超过每分钟的登录确认请求数",
  };
  TooManyAttemptsMinute();
}
class TooManyAttemptsHour extends AuthException{
  @override
  String get name => "TO_MANY_ATTEMPTS_HOUR";

  @override
  Map<String, String> get message => {
    "ru": "Превышено число запросов в час на подтверждение входа",
    "en": "Exceeded number of login requests per hour",
    "zh": "超过每小时的登录请求数",
  };
  TooManyAttemptsHour() : super();
}
class BadEmail extends AuthException{
  @override
  String get name => "BAD_EMAIL";

  @override
  Map<String, String> get message => {
    "ru": "Неверный формат email",
    "en": "Invalid email format",
    "zh": "电子邮件格式无效",
  };
  BadEmail() : super();
}
class CodeError extends AuthException{
  @override
  String get name => "CODE_ERROR";

  @override
  Map<String, String> get message => {
    "ru": "Код не отправлен",
    "en": "Code not sent",
    "zh": "未发送代码",
  };
  CodeError() : super();
}
class NoUser extends AuthException{
  @override
  String get name => "NO_USER";

  @override
  Map<String, String> get message => {
    "ru": "Пользователя с данным email не существует",
    "en": "User with given email does not exist",
    "zh": "具有给定电子邮件的用户不存在",
  };
  NoUser() : super();
}
class BadCode extends AuthException{
  @override
  String get name => "BAD_CODE";

  @override
  Map<String, String> get message => {
    "ru": "Неверный код подтверждения",
    "en": "Invalid confirmation code",
    "zh": "无效的确认码",
  };
  BadCode() : super();
}
class CodeExpire extends AuthException{
  @override
  String get name => "CODE_EXPIRE";

  @override
  Map<String, String> get message => {
    "ru": "Превышено количество попыток ввода кода. Запросите новый код.",
    "en": "Exceeded the number of attempts to enter the code. Request a new code.",
    "zh": "超过尝试输入代码的次数。请求新代码。",
  };
  CodeExpire() : super();
}
class TwoFaError extends AuthException{
  @override
  String get name => "2FA_ERROR";

  @override
  Map<String, String> get message => {
    "ru": "Код двухфакторной аутентификации неверен",
    "en": "Two-factor authentication code is invalid",
    "zh": "两因素验证码不正确",
  };
  TwoFaError() : super();
}
class FailedToUpdate extends AuthException{
  @override
  String get name => "FAILED_TO_UPDATE";

  @override
  Map<String, String> get message => {
    "ru": "Не удалось обновить",
    "en": "Failed to update",
    "zh": "更新失败",
  };
  FailedToUpdate() : super();
}
class IsDelete extends AuthException {
  @override
  String get name => "IS_DELETE";

  @override
  Map<String, String> get message => {
    "ru": "Учетная запись была удалена, вы не сможете использовать этот адрес электронной почты в течение 30 дней с даты удаления.",
    "en": "The account has been deleted, you cannot use this email address within 30 days from the date of deletion.",
    "zh": "该帐户已被删除，您不能在删除之日起30天内使用此电子邮件地址。",
  };
  IsDelete() : super();
}
class Unknown extends AuthException {
  @override
  String get name => "UNKNOWN";

  @override
  Map<String, String> get message => {
    "ru": "Неизвестная ошибка",
    "en": "Unknown error",
    "zh": "未知错误",
  };
  Unknown() : super();
}