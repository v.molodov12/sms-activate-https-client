
import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';

class ServerException extends BaseException {
  @override
  String get name => "server_exception";

  @override
  Map<String, String> get message => {
    "ru": "Проблема соединения с интернетом",
    "en": "Internet connection problem",
    "zh": "互联网连接问题",
  };

  ServerException() : super();
}