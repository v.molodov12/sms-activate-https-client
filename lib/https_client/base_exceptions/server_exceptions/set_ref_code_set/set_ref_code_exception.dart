import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class SetRefCodeException extends ServerException {
  SetRefCodeException() : super();
}

class NoReferrerException extends SetRefCodeException {
  @override
  String get name => "NO_REFERRER";

  @override
  Map<String, String> get message => {
    "ru": "Неправильный формат промокода",
    "en": "Incorrect promo code format",
    "zh": "促销代码格式不正确",
  };

  NoReferrerException() : super();
}
class AccountIsReferredException extends SetRefCodeException {
  @override
  String get name => "ACCOUNT_IS_REFERRED";

  @override
  Map<String, String> get message => {
    "ru": "Аккаунт уже участвует в реферальной программе",
    "en": "The account is already participating in the referral program",
    "zh": "该帐户已参与推荐计划",
  };

  AccountIsReferredException() : super();
}
class AccountIsOldException extends SetRefCodeException {
  @override
  String get name => "ACCOUNT_IS_OLD";

  @override
  Map<String, String> get message => {
    "ru": "Аккаунт не может участвовать в реферальной программе",
    "en": "The account cannot participate in the referral program",
    "zh": "帐户不能参加推荐计划",
  };

  AccountIsOldException() : super();
}
