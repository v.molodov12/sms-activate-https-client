import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class SessionAuthException extends ServerException {
  SessionAuthException() : super();
}

class SessionException extends SessionAuthException {
  @override
  String get name => "SESSION_EXPIRED";

  @override
  Map<String, String> get message => {
    "ru": "Текущая сессия устарела.",
    "en": "The current session is outdated.",
    "zh": "本届会议已经过时。",
  };

  SessionException() : super();
}
class BadSessionException extends SessionAuthException {
  @override
  String get name => "BAD_SESSION";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка сессии",
    "en": "Session error",
    "zh": "会话错误",
  };

  BadSessionException() : super();
}
class AccessDenied extends SessionAuthException {
  @override
  String get name => "ACCESS_DENIED";

  @override
  Map<String, String> get message => {
    "ru": "Текущая сессия недействительна.",
    "en": "The current session is invalid.",
    "zh": "当前会话无效。",
  };

  AccessDenied() : super();
}
