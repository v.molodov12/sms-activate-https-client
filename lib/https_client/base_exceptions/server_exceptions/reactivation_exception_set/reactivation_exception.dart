import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class ReactivationNumberException extends ServerException {
  ReactivationNumberException() : super();
}

class ReactivationException extends ReactivationNumberException {
  @override
  String get name => "reactivation_exception";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка получения данных. Обратитесь в техподдержку",
    "en": "Error getting data. Contact technical support",
    "zh": "获取数据时出错。联系技术支持",
  };

  ReactivationException() : super();
}

class NoAccessToTheNumberException extends ReactivationNumberException {
  @override
  String get name => "no_access_to_the_number_exception";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка получения данных. Обратитесь в техподдержку",
    "en": "Error getting data. Contact technical support",
    "zh": "获取数据时出错。联系技术支持",
  };

  NoAccessToTheNumberException() : super();
}
