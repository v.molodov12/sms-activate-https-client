import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';

class SetPromoCodeException extends ServerException {
  SetPromoCodeException() : super();
}

class WrongPromoIdException extends SetPromoCodeException {
  @override
  String get name => "WRONG_PROMOID";

  @override
  Map<String, String> get message => {
    "ru": "Промокод не указан или введен неправильно",
    "en": "Promo code is not specified, or entered incorrectly",
    "zh": "促销代码未指定，或输入错误",
  };


  WrongPromoIdException() : super();
}
class WrongCodeException extends SetPromoCodeException {
  @override
  String get name => "WRONG_CODE";

  @override
  Map<String, String> get message => {
    "ru": "Неправильный формат промокода",
    "en": "Incorrect promo code format",
    "zh": "促销代码格式不正确",
  };


  WrongCodeException() : super();
}

class NoPromoException extends SetPromoCodeException {
  @override
  String get name => "NO_PROMO";

  @override
  Map<String, String> get message => {
    "ru": "Промокод недействителен",
    "en": "The promo code is not valid",
    "zh": "促销代码无效",
  };


  NoPromoException() : super();
}

class MaxAttemptsExceedException extends SetPromoCodeException {
  @override
  String get name => "MAX_ATTEMPTS_EXCEED";

  @override
  Map<String, String> get message => {
    "ru": "Достигнут лимит активаций промо-кода",
    "en": "The limit of promo code activations has been reached",
    "zh": "促销代码激活的限制已经达到",
  };


  MaxAttemptsExceedException() : super();
}

class DisabledException extends SetPromoCodeException {
  @override
  String get name => "DISABLED";

  @override
  Map<String, String> get message => {
    "ru": "Промокод недействителен",
    "en": "The promo code is invalid",
    "zh": "促销代码无效",
  };


  DisabledException() : super();
}

class ExpiredException extends SetPromoCodeException {
  @override
  String get name => "EXPIRED";

  @override
  Map<String, String> get message => {
    "ru": "Промокод недействителен",
    "en": "The promo code is invalid",
    "zh": "促销代码无效",
  };


  ExpiredException() : super();
}
class NonStartedException extends SetPromoCodeException {
  @override
  String get name => "NOT_STARTED";

  @override
  Map<String, String> get message => {
    "ru": "Промокод недействителен",
    "en": "The promo code is invalid",
    "zh": "促销代码无效",
  };


  NonStartedException() : super();
}
class UsedException extends SetPromoCodeException {
  @override
  String get name => "USED";

  @override
  Map<String, String> get message => {
    "ru": "Промокод уже был использован",
    "en": "The promo code has already been used",
    "zh": "促销代码已被使用",
  };


  UsedException() : super();
}
class NoSpendingException extends SetPromoCodeException {
  @override
  String get name => "NO_SPENDING";

  @override
  Map<String, String> get message => {
    "ru": "Недостаточно потратили на активации",
    "en": "Not enough spent on activation",
    "zh": "没有足够的用于激活",
  };


  NoSpendingException() : super();
}
class IDdNotInListException extends SetPromoCodeException {
  @override
  String get name => "ID_NOT_IN_LIST";

  @override
  Map<String, String> get message => {
    "ru": "Промокод недействителен",
    "en": "The promo code is invalid",
    "zh": "促销代码无效",
  };


  IDdNotInListException() : super();
}
class NoPromoCodeException extends SetPromoCodeException {
  @override
  String get name => "NO_PROMOCODE";

  @override
  Map<String, String> get message => {
    "ru": "Неправильный формат промокода",
    "en": "Incorrect promo code format",
    "zh": "促销代码格式不正确",
  };


  NoPromoCodeException() : super();
}
