import 'package:sms_activate_https_client/https_client/base_exceptions/server_exceptions/server_exception.dart';


class BuyNumberException extends ServerException {
  BuyNumberException();
}

class BadAction extends BuyNumberException {

  @override
  String get name => "BAD_ACTION";

  @override
  Map<String, String> get message => {
    "ru": "Неверное действие",
    "en": "Wrong action",
    "zh": "错误的动作",
  };
  BadAction();
}

class BadService extends BuyNumberException {
  @override
  String get name => "BAD_SERVICE";

  @override
  Map<String, String> get message => {
    "ru": "Неверный сервис",
    "en": "Wrong service",
    "zh": "错误的服务",
  };
  BadService();
}

class BadKey extends BuyNumberException {
  @override
  String get name => "BAD_KEY";

  @override
  Map<String, String> get message => {
    "ru": "Неверный api-ключ",
    "en": "Invalid api key",
    "zh": "无效的 API 密钥",
  };
  BadKey() : super();
}

class ErrorSql extends BuyNumberException {
  @override
  String get name => "ERROR_SQL";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка приложения!",
    "en": "Application error!",
    "zh": "应用程序错误！",
  };
  ErrorSql() : super();
}

class NoBalance extends BuyNumberException {
  @override
  String get name => "NO_BALANCE";

  @override
  Map<String, String> get message => {
    "ru": "Закончился баланс",
    "en": "Ended balance",
    "zh": "期末余额",
  };
  NoBalance() : super();
}

class NoNumbers extends BuyNumberException {
  @override
  String get name => "NO_NUMBERS";

  @override
  Map<String, String> get message => {
    "ru": "Нет номеров",
    "en": "No numbers",
    "zh": "没有数字",
  };
  NoNumbers() : super();
}

class ErrorWhatsApp extends BuyNumberException {
  @override
  String get name => "WHATSAPP_NOT_AVAILABLE";

  @override
  Map<String, String> get message => {
    "ru": "Покупка WhatsApp недоступна. Подробнее на сайте.",
    "en": "WhatsApp purchase is not available. More details on the site.",
    "zh": "WhatsApp 购买不可用。网站上的更多细节。",
  };
  ErrorWhatsApp() : super();

}

class ErrorYoulaMail extends BuyNumberException {
  @override
  String get name => "NO_YULA_MAIL";

  @override
  Map<String, String> get message => {
    "ru": "Для покупки сервисов Mail.ru необходимо иметь на счету более 500 рублей.",
    "en": "To purchase Mail.ru services, you need to have more than 500 rubles on your account.",
    "zh": "要购买 Mail.ru 服务，您的帐户需要有超过 500 卢布。",
  };
  ErrorYoulaMail() : super();
}

class ErrorByServiceBan extends BuyNumberException {
  @override
  String get name => "BANNED";

  @override
  Map<String, String> get message => {
    "ru": "Покупка сервиса запрещена",
    "en": "The purchase of the service is prohibited",
    "zh": "禁止购买该服务",
  };
  ErrorByServiceBan() : super();
}
class BadPassword extends BuyNumberException {
  @override
  String get name => "BAD_PASSWORD";

  @override
  Map<String, String> get message => {
    "ru": "Вам нужно перейти на сайт sms-activate.org и смените пароль",
    "en": "You need to go to the website sms-activate.org and change the password",
    "zh": "你需要去网站sms-activate.org 并更改密码",
  };
  BadPassword() : super();
}
class WrongMaxPrice extends BuyNumberException {
  @override
  String get name => "WRONG_MAX_PRICE";

  @override
  Map<String, String> get message => {
    "ru": "Выбранная цена недоступна для вашего аккаунта.",
    "en": "The selected price is not available for your account.",
    "zh": "所选价格不适用于您的帐户。",
  };
  WrongMaxPrice() : super();
}