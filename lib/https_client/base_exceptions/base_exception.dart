
abstract class BaseException implements Exception {
  BaseException();

  final String name = "base_exception";

  final Map<String, String> message = {
    "ru": "Проблема соединения с интернетом",
    "en": "Internet connection problem",
    "zh": "互联网连接问题",
  };

  Map<String, String> toMap() {
    return {
      'status': 'error',
      'error': name,
      'message': message["en"] ?? "",
    };
  }
}