import 'package:sms_activate_https_client/https_client/base_exceptions/base_exception.dart';

class AppException extends BaseException {

  @override
  String get name => "app_exception";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка приложения!",
    "en": "Application error!",
    "zh": "应用程序错误！",
  };

  AppException() : super();
}