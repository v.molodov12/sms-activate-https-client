import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';

class DDoSException extends AppException{

  @override
  String get name => "ddos_exception";

  @override
  Map<String, String> get message => {
    "ru": "The limit for sending requests has been exceeded.",
    "en": "Internet connection problem",
    "zh": "已超出发送请求的限制。",
  };

  DDoSException() : super();
}