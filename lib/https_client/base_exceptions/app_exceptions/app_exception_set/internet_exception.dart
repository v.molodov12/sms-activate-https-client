import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';

class InternetException extends AppException{
  @override
  String get name => "internet_exception";

  @override
  Map<String, String> get message => {
    "ru": "Нет подключения к Интернету.",
    "en": "There is no internet connection.",
    "zh": "没有互联网连接。",
  };

  InternetException() : super();
}