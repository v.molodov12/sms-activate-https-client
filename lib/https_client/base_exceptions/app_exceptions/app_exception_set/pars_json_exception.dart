import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';

class ParsJsonException extends AppException {

  @override
  String get name => "pars_json_exception";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка синтаксического анализа Json.",
    "en": "Json parsing error.",
    "zh": "Json解析错误。",
  };

  ParsJsonException();
}