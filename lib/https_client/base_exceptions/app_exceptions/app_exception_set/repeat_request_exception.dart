
import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';

class RepeatRequestException extends AppException {

  @override
  String get name => "repeat_request_failed";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка при отправке повторного запроса.",
    "en": "Error sending a repeated request.",
    "zh": "发送重复请求时出错。",
  };

  RepeatRequestException() : super();
}