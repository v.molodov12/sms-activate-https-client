import 'package:sms_activate_https_client/https_client/base_exceptions/app_exceptions/app_exception.dart';

class UpdateCookieException extends AppException {

  @override
  String get name => "request_update_cookie_failed";

  @override
  Map<String, String> get message => {
    "ru": "Ошибка обновления ключей доступа.",
    "en": "Error updating access keys.",
    "zh": "更新访问密钥时出错。",
  };

  UpdateCookieException() : super();
}