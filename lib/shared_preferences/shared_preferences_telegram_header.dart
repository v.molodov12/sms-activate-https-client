import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesUserData {

  static const String _userDataKey ="user_data";
  static const String _dataUrlKey ="url_data";

  static Future<bool> saveUserData(final String userData) async {
    final sp = await SharedPreferences.getInstance();
    return sp.setString(_userDataKey, userData);
  }

  static Future<String> getUserData() async {
    final sp = await SharedPreferences.getInstance();
    return sp.getString(_userDataKey)??"";
  }

  static Future<bool> saveDataUrl(final String dataUrl) async {
    final sp = await SharedPreferences.getInstance();
    return sp.setString(_dataUrlKey, dataUrl);
  }

  static Future<String> getDataUrl() async {
    final sp = await SharedPreferences.getInstance();
    return sp.getString(_dataUrlKey)??"";
  }


  static Future<void> clear() async {
    final sp = await SharedPreferences.getInstance();
    await sp.clear();
  }
}
